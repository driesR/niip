#include "AS726X.h"
AS726X sensor;

void setup() {
  sensor.begin();
}

void loop() {
  sensor.takeMeasurements();
  sensor.printMeasurements();//Prints out all measurements (calibrated)
}
