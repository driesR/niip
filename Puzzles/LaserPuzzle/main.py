from machine import Pin
import socket
import time
import pycom
import json
import uos
import _thread

IP_ADDRESS = "192.168.16.175"
# IP_ADDRESS = "192.168.10.141"
PORT = 8082

pTEK0 = Pin('P5', mode=Pin.IN, pull=Pin.PULL_UP)
pTEK1 = Pin('P6', mode=Pin.IN, pull=Pin.PULL_UP)
pTEK2 = Pin('P7', mode=Pin.IN, pull=Pin.PULL_UP)
pTEK3 = Pin('P9', mode=Pin.IN, pull=Pin.PULL_UP)
pTEK4 = Pin('P10', mode=Pin.IN, pull=Pin.PULL_UP)
pTEK5 = Pin('P11', mode=Pin.IN, pull=Pin.PULL_UP)

COLORS = ["green", "yellow", "white", "cyan", "purple", "red"]

def connect_to_server():
    while True:
        try:
            s = socket.socket()
            s.connect((IP_ADDRESS, PORT))
            
            print("connection to server established")
            return s

        except socket.error as err:
            print("Cannot connect to server", str(err))
            time.sleep(2)

        except Exception as err:
            print("Unknown error", str(err))

def register_with_server(sock: socket.socket):
    registration_message = {
        "type": "puzzle_registration",
        "message": {
            "puzzle_type": "laser"
        }
    }

    send_to_server(sock, registration_message)

    response = receive_from_server(sock)

    if response["type"] == "puzzle_registration_ok":
        print("Successfully registered with gameserver")

        pycom.rgbled(0xcccc00)

def send_to_server(sock: socket.socket, message):
    data = json.dumps(message).encode()

    data_send = False
    while not data_send:
        try:
            sock.send(data)
            data_send = True
            print("data send:", data)

            if len(data) >= 1024:
                print("overflow:", len(data))

        except socket.error as err:
            print("Cannot send data")
            connect_to_server()
            time.sleep(2)

def receive_from_server(sock: socket.socket):
    data_received = False
    message = None

    while not data_received:
        try:
            data = sock.recv(1024)
            data_received = True
            print("data received:", data)
            message = json.loads(data.decode())

        except socket.error as err:
            print("Cannot receive data")
            connect_to_server()
            time.sleep(2)
        
    return message


def generate_random_sequence(length):
    seq = []

    for i in range(length):
        rand = uos.urandom(1)[0] / 256
        
        if rand < (1/6):
            seq.append(COLORS[0])
        elif rand < (2/6):
            seq.append(COLORS[1])
        elif rand < (3/6):
            seq.append(COLORS[2])
        elif rand < (4/6):
            seq.append(COLORS[3])
        elif rand < (5/6):
            seq.append(COLORS[4])
        elif rand < 1:
            seq.append(COLORS[5])

    return seq


def game_loop(sock: socket.socket, game_id):
    correct_sequence = generate_random_sequence(8)

    sequence_generated_event = {
        "type": "sequence_generated",
        "message": {
            "sequence": correct_sequence
        }
    }
    
    send_to_server(sock, sequence_generated_event)

    sensor_activated_event = {
        "type": "sensor_activated",
        "message": {
            "game_id": game_id,
            "puzzle": "laser",
            "color": None
        }
    }

    
    active = True
    sensor_activated = False
    color = None
    current_sequence = []
    reset_done = True
    while active:
        if pTEK0():
            sensor_activated = True
            print("0")
            pycom.rgbled(0x00ff00) # green

            sensor_activated_event["message"]["color"] = "green"
            color = COLORS[0]
        elif pTEK1():
            sensor_activated = True
            print("1")
            pycom.rgbled(0xffff00) # yellow

            sensor_activated_event["message"]["color"] = "yellow"
            color = COLORS[1]

        elif pTEK2():
            sensor_activated = True
            print("2")
            pycom.rgbled(0xffffff) # white

            sensor_activated_event["message"]["color"] = "white"
            color = COLORS[2]

        elif pTEK3():
            sensor_activated = True
            print("3")
            pycom.rgbled(0x00ffff) # cyan

            sensor_activated_event["message"]["color"] = "cyan"
            color = COLORS[3]

        elif pTEK4():
            sensor_activated = True
            print("4")
            pycom.rgbled(0xff00ff) # purple

            sensor_activated_event["message"]["color"] = "purple"
            color = COLORS[4]

        elif pTEK5():
            sensor_activated = True
            print("5")
            pycom.rgbled(0xff0000) # red

            sensor_activated_event["message"]["color"] = "red"
            color = COLORS[5]


        if sensor_activated:
            sensor_activated = False

            if not reset_done:
                print("reading message")
                message = receive_from_server(sock)

                if message["type"] == "puzzle_reset":
                    last_color  =current_sequence[-1]
                    current_sequence.clear()
                    current_sequence.append(last_color)
                    reset_done = True
            else:
                reset_done = False
            
            send_to_server(sock, sensor_activated_event)
            current_sequence.append(color)
            print(current_sequence)
            
            time.sleep(1)
            pycom.rgbled(0x000000)

            if current_sequence == correct_sequence:
                active = False

                puzzle_solved_event = {
                    "type": "puzzle_solved"
                }

                send_to_server(sock, puzzle_solved_event)
                print("puzzle_solved")
            else:
                time.sleep(1)

def main():
    sock = connect_to_server()
    register_with_server(sock)
    pycom.heartbeat(False)

    while True:
        msg = receive_from_server(sock)

        if msg["type"] == "puzzle_start":
            game_id = msg["message"]["game_id"]
            game_loop(sock, game_id)
    

_thread.start_new_thread(main, ())
