from network import WLAN, Server
import machine
import pycom
import time

AP_SSID = "DriesEnMaarten"
AP_PASSWORD = '123456789'

SSID = "MaartenEnDries-MMER"
SSID_PASSWORD = "MaartenEnDries-MMER"

#SSID = "EDM-Guest"
#SSID_PASSWORD = "dulosi68"

wlan = WLAN(mode=WLAN.STA)

def setup_FTP():
    FTP = Server()
    FTP.init()
    if FTP.isrunning():
        print("FTP server up and running")
        return True
    
    return False


def setup_wifi_connection():
    nets = wlan.scan()

    for net in nets:
        if net.ssid == SSID:
            print('Network found!')
            wlan.connect(net.ssid, auth=(net.sec, SSID_PASSWORD), timeout=5000)
            while not wlan.isconnected():
                machine.idle()  # save power while waiting
            print('WLAN connection succeeded!')
            print(wlan.ifconfig(0))
            print(wlan.isconnected())
            return True

    return False


def setup_wifi_ap():
    wlan.init(mode=WLAN.STA_AP, ssid=AP_SSID, auth=(WLAN.WPA2, AP_PASSWORD))
    print("AP setup completed")
    print(wlan.ifconfig(1))

pycom.heartbeat(False)

pycom.rgbled(0xff0000)
# setup_wifi_ap()
pycom.rgbled(0xff8000)
if setup_wifi_connection():
    pycom.rgbled(0xffff00)
if setup_FTP():
    pycom.rgbled(0x00ff00)
