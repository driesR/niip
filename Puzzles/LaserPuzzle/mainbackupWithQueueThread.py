import pycom
import time
from machine import Pin

import _thread

#import queue

pTEK1 = Pin('P10', mode=Pin.IN, pull=Pin.PULL_UP)
pTEK2 = Pin('P11', mode=Pin.IN, pull=Pin.PULL_UP)
pTEK3 = Pin('P12', mode=Pin.IN, pull=Pin.PULL_UP)
pTEK4 = Pin('P13', mode=Pin.IN, pull=Pin.PULL_UP)
pTEK5 = Pin('P14', mode=Pin.IN, pull=Pin.PULL_UP)
pycom.heartbeat(False)

class Queue:
    def __init__(self):
        self.items = []

    def isEmpty(self):
        return self.items == []

    def enqueue(self, item):
        self.items.insert(0,item)

    def dequeue(self):
        return self.items.pop()

    def size(self):
        return len(self.items)

q = Queue()
q.enqueue('hello')
q.enqueue('dog')
q.enqueue(3)

print(q.dequeue())
print(q.dequeue())

'''
q = queue.Queue()
#insert items at the end of the queue 
for x in range(4):
    q.put(str(x))
#remove items from the head of the queue 
while not q.empty():
    print(q.get(), end=" ")
print("\n")
    '''


def checksensor():
    print("check")
    while True:
        if pTEK1():
            print("1")
            q.enqueue(1)
            time.sleep(2)
        if pTEK2():
            q.enqueue(2)
            time.sleep(2)
'''
def activateRGB():
    while True:
        #when filled, go and empty& display
        while not q.empty():
            varx = q.get()
            if(varx = 1):
                pycom.rgbled(0x7f0000) # red
                time.sleep(2)
            elif(varx = 2):
                pycom.rgbled(0x00007f) # blue
                time.sleep(2)
            else:
                pycom.rgbled(0x007f00) # green
                time.sleep(2)
        time.sleep(1)
'''
_thread.start_new_thread(checksensor,())
#_thread.start_new_thread(activateRGB,())
while True:
    pass

pycom.heartbeat(True)