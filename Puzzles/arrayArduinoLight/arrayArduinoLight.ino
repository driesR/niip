int sensorPin0 = A0;  
int sensorPin1 = A1; 
int sensorPin2 = A2;  
int sensorPin3 = A3;  
int sensorPin4 = A4;   
int sensorPin5 = A5; 

int sensorValue0 = 0;
int sensorValue1 = 0;
int sensorValue2 = 0;
int sensorValue3 = 0;
int sensorValue4 = 0;
int sensorValue5 = 0;
//int buzzPin=11;


void setup() {
  Serial.begin(9600);
  
  //pinMode(buzzPin, OUTPUT);
  pinMode(sensorPin0, INPUT);
  pinMode(sensorPin1, INPUT);
  pinMode(sensorPin2, INPUT);
  pinMode(sensorPin3, INPUT);
  pinMode(sensorPin4, INPUT);
  pinMode(sensorPin5, INPUT);

  pinMode(8, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(2, OUTPUT);
  //pinMode(1, OUTPUT);
  //pinMode(0, OUTPUT);
  digitalWrite(8,LOW); //is 0 voltage, to receive


  pinMode(LED_BUILTIN, OUTPUT);
  
}

void loop() {
sensorValue0 = analogRead(sensorPin0);   
sensorValue1 = analogRead(sensorPin1);   
sensorValue2 = analogRead(sensorPin2);   
sensorValue3 = analogRead(sensorPin3);   
sensorValue4 = analogRead(sensorPin4);  
sensorValue5 = analogRead(sensorPin5); 

if(sensorValue0>=900){digitalWrite(2,HIGH); digitalWrite(LED_BUILTIN, HIGH);Serial.println(sensorValue0);}
else{digitalWrite(2,LOW);}
if(sensorValue1>=900){digitalWrite(3,HIGH); digitalWrite(LED_BUILTIN, HIGH);Serial.println(sensorValue1);}
else{digitalWrite(3,LOW);}
if(sensorValue2>=900){digitalWrite(4,HIGH); digitalWrite(LED_BUILTIN, HIGH);Serial.println(sensorValue2);}
else{digitalWrite(4,LOW);}
if(sensorValue3>=900){digitalWrite(5,HIGH); digitalWrite(LED_BUILTIN, HIGH);Serial.println(sensorValue3);}
else{digitalWrite(5,LOW);}
if(sensorValue4>=900){digitalWrite(6,HIGH); digitalWrite(LED_BUILTIN, HIGH);Serial.println(sensorValue4);}
else{digitalWrite(6,LOW);}
if(sensorValue5>=900){digitalWrite(7,HIGH); digitalWrite(LED_BUILTIN, HIGH);Serial.println(sensorValue5);}
else{digitalWrite(7,LOW);}

digitalWrite(LED_BUILTIN, LOW);
 
}
