#include "AS726X.h"
AS726X sensor;

byte GAIN = 2;
byte MEASUREMENT_MODE = 0;

void setup() {
    sensor.begin(Wire, GAIN, MEASUREMENT_MODE);
}

void loop() {
    sensor.takeMeasurements();
    sensor.printMeasurements();
}
