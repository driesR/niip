from flask import Blueprint
from flask_restful import Api

from apiserver.resources.player import Players, Player
from apiserver.resources.game import Games, Game
from apiserver.resources.puzzle import Puzzles, Puzzle

api_blueprint = Blueprint("api", __name__)
api = Api(api_blueprint)

api.add_resource(Games, "/games")
api.add_resource(Game, "/game/<int:game_id>")

api.add_resource(Players, "/players")
api.add_resource(Player, "/player/<int:player_id>")

api.add_resource(Puzzles, "/puzzles")
api.add_resource(Puzzle, "/puzzle/<int:puzzle_id>")


