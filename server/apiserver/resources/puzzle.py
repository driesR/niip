from flask import request
from flask_restful import Resource
from marshmallow import ValidationError
from sqlalchemy.exc import IntegrityError
from werkzeug.exceptions import HTTPException

from apiserver.models.puzzle import puzzle_schema, PuzzleModel
from db_ma import db


class Puzzles(Resource):
    def get(self):
        try:
            puzzle_entries = PuzzleModel.query.all()

            data, _ = puzzle_schema.dump(puzzle_entries, many=True)

            return data, 200

        except ValidationError as err:
            return {
                "message": "Failed to get a list of puzzles",
                "details": err.messages
            }, 500

        except Exception as err:
            return {
                "message": "Failed to get a list of puzzles",
                "details": "Unknown server error: " + err
            }, 500

    def post(self):
        try:
            puzzle_entry, _ = puzzle_schema.load(request.get_json())

            db.session.add(puzzle_entry)
            db.session.commit()

            return {
                "message": "Successfully created a new puzzle"
            }, 201, {"Location": "/puzzle/{}".format(puzzle_entry.puzzle_id)}

        except ValidationError as err:
            db.session.rollback()
            return {
                "message": "Failed to create a new puzzle",
                "details": err.messages
            }, 400

        except IntegrityError as err:
            db.session.rollback()
            return {
                "message": "Failed to create a new puzzle",
                "details": "Database error: please check your input."
            }, 400

        except HTTPException as err:
            db.session.rollback()
            return {
                "message": "Failed to create a new puzzle",
                "details": err.name + ": " + err.description
            }, err.code

        except Exception as err:
            db.session.rollback()
            return {
                "message": "Failed to create a new puzzle",
                "details": "Unknown server error: " + err
            }, 500


class Puzzle(Resource):
    def get(self, puzzle_id):
        try:
            puzzle_entry = PuzzleModel.query.get(puzzle_id)

            data, _ = puzzle_schema.dump(puzzle_entry)

            if puzzle_entry is None:
                return {
                    "message": "Failed to get the puzzle with id {}".format(puzzle_id),
                    "details": "The puzzle doesn't exist"
                }, 404
            else:
                return data, 200

        except ValidationError as err:
            return {
                "message": "Failed to get the puzzle with id {}".format(puzzle_id),
                "details": err.messages
            }

        except Exception as err:
            return {
                "message": "Failed to get the puzzle with id {}".format(puzzle_id),
                "details": "Unknown server error: " + err
            }, 500

    def delete(self, puzzle_id):
        try:
            puzzle_entry = PuzzleModel.query.get(puzzle_id)

            if puzzle_entry is None:
                return {
                    "message": "Failed to delete the puzzle with id {}".format(puzzle_id),
                    "details": "The puzzle doesn't exist"
                }, 404

            db.session.delete(puzzle_entry)
            db.session.commit()

        except IntegrityError as err:
            db.session.rollback()
            return {
                "message": "Failed to delete the puzzle with id {}".format(puzzle_id),
                "details": "Database error: please check you input"
            }, 400

        except Exception as err:
            db.session.rollback()
            return {
                "message": "Failed to delete the puzzle with id {}".format(puzzle_id),
                "details": "Unknown server error: " + err
            }, 500