import json

import socketio
from flask import request
from flask_restful import Resource
from marshmallow import ValidationError
from sqlalchemy.exc import IntegrityError
from werkzeug.exceptions import HTTPException

from apiserver.models.game import game_schema, GameModel
from db_ma import db


class Games(Resource):
    def get(self):
        try:
            if request.args.get("game_name") is None:
                game_entries = GameModel.query.all()
            else:
                game_entries = GameModel.query.filter_by(game_name=request.args.get("game_name"))

            data, _ = game_schema.dump(game_entries, many=True)

            return data, 200

        except ValidationError as err:
            return {
                "message": "Failed to get a list of games",
                "details": err.messages
            }, 500

        except Exception as err:
            return {
                "message": "Failed to get a list of games",
                "details": "Unknown server error: " + err
            }, 500

    def post(self):
        try:
            game_entry, _ = game_schema.load(request.get_json())

            if GameModel.query.filter_by(game_name=game_entry.game_name).first():
                return {
                    "message": "Failed to create a new game",
                    "details": "A game with the same name already exists"
                }, 400

            db.session.add(game_entry)
            db.session.commit()

            sio_client = socketio.Client()
            sio_client.connect("http://localhost:5000", namespaces=["/test"])

            msg = {
                "game_id": game_entry.game_id
            }
            sio_client.emit("create_game", json.dumps(msg), namespace="/test")

            return {
                "message": "Successfully created a new game"
            }, 201, {"Location": "/game/{game_id}".format(game_id=game_entry.game_id)}

        except ValidationError as err:
            db.session.rollback()
            return {
                "message": "Failed to create a new game",
                "details": err.messages
            }, 400

        except IntegrityError as err:
            db.session.rollback()
            return {
                "message": "Failed to create a new game",
                "details": "Database error: please check your input."
            }, 400

        except HTTPException as err:
            db.session.rollback()
            return {
                "message": "Failed to create a new game",
                "details": err.name + ": " + err.description
            }, err.code

        except Exception as err:
            db.session.rollback()
            return {
                "message": "Failed to create a new game",
                "details": "Unknown server error: " + err
            }, 500


class Game(Resource):
    def get(self, game_id):
        try:
            game_entry = GameModel.query.get(game_id)

            data, _ = game_schema.dump(game_entry)

            if game_entry is None:
                return {
                    "message": "Failed to get the game with id {game_id}".format(game_id=game_id),
                    "details": "The game doesn't exist"
                }, 404
            else:
                return data, 200
        
        except ValidationError as err:
            return {
                "message": "Failed to get the game with id {game_id}".format(game_id=game_id),
                "details": err.messages
            }

        except Exception as err:
            return {
                "message": "Failed to get the game with id {game_id}".format(game_id=game_id),
                "details": "Unknown server error: " + err
            }, 500

    def put(self, game_id):
        try:
            updated_entry, _ = game_schema.load(request.get_json())
            game_entry = GameModel.query.get(game_id)

            if game_entry is None:
                return {
                    "message": "Failed to update the game with id {game_id}".format(game_id=game_id),
                    "details": "The game doesn't exists"
                }, 404
            
            game_entry.update(updated_entry)
            db.session.commit()

        except ValidationError as err:
            db.session.rollback()
            return {
                "message": "Failed to update the game with id {game_id}".format(game_id=game_id),
                "details": err.messages
            }, 400

        except IntegrityError as err:
            db.session.rollback()
            return {
                "message": "Failed to update the game with id {game_id}".format(game_id=game_id),
                "details": "Database error: please check your input."
            }, 400

        except Exception as err:
            db.session.rollback()
            return {
                "message": "Failed to update the game with id {game_id}".format(game_id=game_id),
                "details": "Unknown server error: " + err
            }, 500

    def delete(self, game_id):
        try:
            game_entry = GameModel.query.get(game_id)

            if game_entry is None:
                return {
                    "message": "Failed to delete the game with id {game_id}".format(game_id=game_id),
                    "details": "The game doesn't exist"
                }, 404

            db.session.delete(game_entry)
            db.session.commit()

        except IntegrityError as err:
            db.session.rollback()
            return {
                "message": "Failed to delete the game with id {game_id}".format(game_id=game_id),
                "details": "Database error: please check you input"
            }, 400

        except Exception as err:
            db.session.rollback()
            return {
                "message": "Failed to delete the game with id {game_id}".format(game_id=game_id),
                "details": "Unknown server error: " + err
            }, 500
