from flask import request
from flask_restful import Resource
from marshmallow import ValidationError
from sqlalchemy.exc import IntegrityError
from werkzeug.exceptions import HTTPException

from apiserver.models.player import player_schema, PlayerModel
from apiserver.models.game import GameModel
from db_ma import db


class Players(Resource):
    def get(self):
        try:
            player_entries = PlayerModel.query.all()

            data, _ = player_schema.dump(player_entries, many=True)

            return data, 200

        except:
            return "", 500

    def post(self):
        try:
            request_dict = request.get_json()

            game_info = GameModel.query.filter_by(game_name=request_dict["game"]["game_name"]).first()

            if game_info is None or game_info.password != request_dict["game"]["password"]:
                return {
                    "message": "Failed to create a new player",
                    "details": "The game doesn't exist or you entered an incorrect password"
                }, 400
            
            player_entry, _ = player_schema.load(request_dict["player"])
            player_entry.game_id = game_info.game_id

            db.session.add(player_entry)
            db.session.commit()
            
            return {
                "message": "Successfully created a new player",
                "details": {"player_id": player_entry.player_id}
            }, 201, {"Location": "/player/{player_id}".format(player_id=player_entry.player_id)}

        except ValidationError as err:
            db.session.rollback()
            return {
                "message": "Failed to create new player",
                "details": err.messages
            }, 400

        except IntegrityError as err:
            db.session.rollback()
            return {
                "message": "Failed to create a new player",
                "details": "Database error: please check your input."
            }, 400

        except HTTPException as err:
            db.session.rollback()
            return {
                "message": "Failed to create a new player",
                "details": err.name + ": " + err.description
            }, err.code

        except Exception as err:
            return {
                "message": "Failed to create a new player",
                "details": "Unknown server error: " + err
            }, 500


class Player(Resource):
    def get(self, player_id):
        try:
            player_entry = PlayerModel.query.get(player_id)

            data, _ = player_entry.dump(player_entry)

            if player_entry is None:
                return "", 404

            else:
                return data, 200

        except:
            return "", 500
