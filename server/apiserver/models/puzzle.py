from db_ma import db, ma


class PuzzleModel(db.Model):
    puzzle_id = db.Column(db.Integer, primary_key=True)
    puzzle_type = db.Column(db.String(30))


class PuzzleSchema(ma.ModelSchema):
    class Meta:
        model = PuzzleModel
        dump_only = ["id"]


puzzle_schema = PuzzleSchema(strict=True)