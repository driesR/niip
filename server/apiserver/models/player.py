from db_ma import db, ma


class PlayerModel(db.Model):
    player_id = db.Column(db.Integer, primary_key=True)
    player_name = db.Column(db.String(30), nullable=False)
    role = db.Column(db.Enum("player", "mole"), nullable=False, default="player")
    game_id = db.Column(db.Integer, db.ForeignKey("game_model.game_id"), nullable=False)


class PlayerSchema(ma.ModelSchema):
    class Meta:
        model = PlayerModel
        dump_only = ["id", "role", "game_id"]


player_schema = PlayerSchema(strict=True)