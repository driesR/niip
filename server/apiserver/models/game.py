from db_ma import db, ma


class GameModel(db.Model):
    game_id = db.Column(db.Integer, primary_key=True)
    game_name = db.Column(db.String(30), unique=True, nullable=False)
    password = db.Column(db.String(30), nullable=False)
    number_of_players = db.Column(db.Integer, nullable=False)
    active = db.Column(db.Boolean, nullable=False, default=True)

    players = db.relationship("PlayerModel", backref="game", lazy=True)


class GameSchema(ma.ModelSchema):
    class Meta:
        model = GameModel
        dump_only = ["game_id"]
        load_only = ["password"]


game_schema = GameSchema(strict=True)
