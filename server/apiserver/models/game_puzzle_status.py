from db_ma import db

game_puzzle_status = db.Table("game_puzzle_status",
    db.Column("game_id", db.Integer, db.ForeignKey("game.game_id"), primary_key=True),
    db.Column("puzzle_id", db.Integer, db.ForeignKey("puzzle.puzzle_id"), primary_key=True),
    db.Column("status", db.Enum(["unsolved", "solved"]), primary_key=True)
)
