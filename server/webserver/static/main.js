function create_new_game() {
    f = document.getElementById("create_new_game_form")
    game_name = document.getElementById("game_name")
    game_password = document.getElementById("game_password")
    number_of_players = document.getElementById("number_of_players")

    data = {
        "game_name": game_name.value,
        "password": game_password.value,
        "number_of_players": number_of_players.value
    }

    $.ajax({
        type: "POST",
        url: "/api/games",
        data: JSON.stringify(data),
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            $.ajax({
                type: "GET",
                url: "/api/games",
                dataType: "json",
                success: function (response) {
                    new_rows = "<tbody>"
                    for (const game of response) {
                        new_rows += "<tr><td>" + game.game_name + "</td><td>" + game.active + "</td></tr>"
                    }
                    new_rows += "</tbody>"

                    $("#game_table_body").replaceWith(new_rows);
                    $("#CreateNewGame").modal("hide")
                }
            });
        }
    });

    return false
}