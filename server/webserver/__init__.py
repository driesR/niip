from urllib.parse import urljoin

import requests
from flask import Blueprint, render_template

webserver_blueprint = Blueprint("webserver", __name__, template_folder="templates", static_folder="static", static_url_path="/webserver/static")

api_base_url = "http://localhost:5000/api/"

@webserver_blueprint.route("/")
def main_page():
    r = requests.get(urljoin(api_base_url, "games"))
    games = r.json()
    return render_template("main.html", games=games)
