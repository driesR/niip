from gameserver.puzzles.puzzle import Puzzle


class QuizPuzzle(Puzzle):
    # def __init__(self, puzzle_type: str):
        # super().__init__(puzzle_type, None)

    def run(self):
        print("Quizpuzzle started")

        TrickQuestionsArray = []
        TrickQuestionsArray.append("What was Justin Bieber's first top 10 hit?")
        TrickQuestionsArray.append("What country has the third biggest military?")
        TrickQuestionsArray.append("In which country took the D-Day offensive place?")
        TrickQuestionsArray.append("Who was the EuroVisionSong winner 2019?")
        TrickQuestionsArray.append("Who was the first American in space?")
        TrickQuestionsArray.append("What is the KKK")
        TrickQuestionsArray.append("Who is the drummer of Metallica?")
        TrickQuestionsArray.append("In which country did Shakespeare s Hamlet live?")
        TrickQuestionsArray.append("Which Egyptian woman was considered as the god of agriculture? She married her brother and together they had a son: Horus.")
        TrickQuestionsArray.append("Who said \"I think therefore I am\"?")
        
        TrickAnswerArray = []
        TrickAnswerArray.append("One Time")
        TrickAnswerArray.append("China")
        TrickAnswerArray.append("France")
        TrickAnswerArray.append("The Netherlands")
        TrickAnswerArray.append("Alan Shepard")
        TrickAnswerArray.append("Ku Klux Klan")
        TrickAnswerArray.append("Lars Ulrich")
        TrickAnswerArray.append("Denmark")
        TrickAnswerArray.append("ISIS")
        TrickAnswerArray.append("Rene Descartes")

        QuestionsArray = []
        QuestionsArray.append("How many presidents have there been in the USA")
        QuestionsArray.append("How long did WW2 last?")
        QuestionsArray.append("What is the lightest existing metal?")
        QuestionsArray.append("What were the first words ever spoken on the telephone by Alexander Graham Bell")
        QuestionsArray.append("Which plant does the Canadian flag contain?")
        QuestionsArray.append("how much is the squareroot of 44100")
        QuestionsArray.append("Fill in the quote of Napoleon Bonaparte. An army marches on its ...")
        QuestionsArray.append("Which French king was called the Sun King?")
        QuestionsArray.append("What is the organ that is affected when one is suffering from hepatitis?")
        QuestionsArray.append("What was in England the northern frontier of the Roman Empire?")

        AnswerArray1 = []
        AnswerArray1.append("45")
        AnswerArray1.append("6years")
        AnswerArray1.append("Aluminium")
        AnswerArray1.append("Mr. Watson, come here, I want to see you.")
        AnswerArray1.append("Maple")
        AnswerArray1.append("210")
        AnswerArray1.append("Stomach")
        AnswerArray1.append("Louis XIV")
        AnswerArray1.append("Liver")
        AnswerArray1.append("Hadrian's wall")

        AnswerArray2 = []
        AnswerArray2.append("40")
        AnswerArray2.append("5years")
        AnswerArray2.append("Silver")
        AnswerArray2.append("It works!!")
        AnswerArray2.append("Oak")
        AnswerArray2.append("201")
        AnswerArray2.append("Feet")
        AnswerArray2.append("Alexander the great")
        AnswerArray2.append("Kidneys")
        AnswerArray2.append("Chinese wall")

        AnswerArray3 = []
        AnswerArray3.append("588")
        AnswerArray3.append("4Years")
        AnswerArray3.append("Tin")
        AnswerArray3.append("Roger")
        AnswerArray3.append("Willow")
        AnswerArray3.append("404")
        AnswerArray3.append("Mind")
        AnswerArray3.append("Caesar")
        AnswerArray3.append("Skin")
        AnswerArray3.append("Heralds wall")

        AnswerArray4 = []
        AnswerArray4.append("58")
        AnswerArray4.append("25years")
        AnswerArray4.append("Copper")
        AnswerArray4.append("Does it work?")
        AnswerArray4.append("Yew")
        AnswerArray4.append("200")
        AnswerArray4.append("Toes")
        AnswerArray4.append("henry")
        AnswerArray4.append("Eyes")
        AnswerArray4.append("Zack Efron")
        AnswerArray4.append("Skin")

        