import json
import socket
import threading
import time


class Puzzle(threading.Thread):
    def __init__(self, puzzle_type: str, sock: socket.socket):
        super().__init__()
        self.puzzle_type = puzzle_type
        self.sock = sock

        self.players = []

        self.game = None
        self.game_id = 0

    def set_game(self, game_id, game):
        self.game_id = game_id
        self.game = game

    def add_player(self, player_id):
        self.players.append(player_id)
        return len(self.players)

    def send_to_puzzle(self, message):
        data = json.dumps(message).encode()

        data_send = False
        while not data_send:
            try:
                self.sock.send(data)
                data_send = True
                print("data send:", data)

            except socket.error as err:
                print("Cannot send data")
                time.sleep(2)

    def receive_from_puzzle(self):
        data_received = False
        message = None

        while not data_received:
            try:
                data = self.sock.recv(1024)
                data_received = True
                print("data received:", data)
                message = json.loads(data.decode())

            except socket.error as err:
                print("Cannot receive data")
                time.sleep(2)

        return message
