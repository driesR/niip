import socket

from gameserver.puzzles.puzzle import Puzzle


class LaserPuzzle(Puzzle):
    def __init__(self, puzzle_type: str, sock: socket.socket):
        super().__init__(puzzle_type, sock)

        self.puzzle_reset = False

    def reset_puzzle(self):
        self.puzzle_reset = True

    def run(self):
        print("Laserpuzzle thread started")
        puzzle_start_event = {
            "type": "puzzle_start",
            "message": {
                "game_id": self.game_id
            }
        }

        self.send_to_puzzle(puzzle_start_event)

        active = True
        while active:
            message = self.receive_from_puzzle()

            if message["type"] == "sequence_generated":
                player_message = {
                    "colors": message["message"]["sequence"]
                }

                self.game.send_to_players("laser_puzzle_start", player_message, broadcast=True)

            elif message["type"] == "sensor_activated":
                player_message = {
                    "laser_color": message["message"]["color"]
                }

                self.game.send_to_players("laser_puzzle_color_listener", player_message, broadcast=True)

                if self.puzzle_reset:
                    self.send_to_puzzle({"type": "puzzle_reset"})
                    self.puzzle_reset = False
                else:
                    self.send_to_puzzle({"type": "puzzle_continue"})

            elif message["type"] == "puzzle_solved":
                self.game.send_to_players("laser_puzzle_verification", "", broadcast=True)
                active = False

            else:
                print("unknown type:", message["type"])
