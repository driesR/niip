import random
import threading
import json

import flask_socketio
import requests

from db_ma import socketio

api_address = "http://localhost:5000/api"


class Game(threading.Thread, flask_socketio.Namespace):
    def __init__(self, namespace, game_id):
        flask_socketio.Namespace.__init__(self, namespace)
        threading.Thread.__init__(self)

        api_response = requests.get(api_address + "/game/{}".format(game_id))

        if api_response.status_code == 200:
            content = api_response.json()
            self.game_id = game_id
            self.players = []
            self.player_lock = threading.Lock()
            self.mole = None
            self.max_number_of_players = content["number_of_players"]
            self.puzzles = {}
            self.puzzle_lock = threading.Lock()

            self.all_players_logged_in = threading.Event()
            self.game_completed = threading.Event()

    def add_puzzle(self, puzzle_type, puzzle):
        # with self.puzzle_lock:
        self.puzzles[puzzle_type] = puzzle
        puzzle.set_game(self.game_id, self)

    def add_player(self, message, sid):
        if self.all_players_logged_in.is_set():
            return "player_registration_failed", "Max number of players already reached"

        api_response = requests.post(api_address + "/players", json=message)

        if api_response.status_code != 201:
            return "player_registration_failed", api_response.json()

        content = api_response.json()
        with self.player_lock:
            self.players.append((content["details"]["player_id"], sid))

            if self.max_number_of_players == len(self.players):
                self.all_players_logged_in.set()

        response = {
            "player_id": api_response.json()["details"]["player_id"],
            "game_id": self.game_id,
            "max_number_of_players": self.max_number_of_players
        }

        return "player_registration_successful", response

    def pick_mole(self):
        with self.player_lock:
            self.mole = random.choice(self.players)
            self.players.remove(self.mole)

            # requests.put(api_address + "/player/{}".format(self.mole))

            socketio.emit("role assigned", "mole", room=self.mole[1])

            for player in self.players:
                socketio.emit("role assigned", "player", room=player[1])

            print("mole picked, players notified")

    def on_player_registration_puzzle(self, data):
        msg = json.loads(data)

        with self.puzzle_lock:
            if msg["puzzle_name"] == "laser":
                puzzle = self.puzzles["laser"]
                number_of_players = puzzle.add_player(msg["player_id"])

                flask_socketio.emit("player_laser_login", {
                    "total_players_logged_in": number_of_players
                }, broadcast=True)

                if number_of_players == self.max_number_of_players:
                    print("laser_puzzle_start")
                    puzzle.start()

            # elif msg["puzzle_name"] == "quiz":
            #     puzzle = QuizPuzzle("quiz")
            #     self.add_puzzle("quiz", puzzle)
            #
            #     number_of_players = puzzle.add_player(msg["player_id"])
            #
            #     flask_socketio.emit("player_quiz_login", {
            #         "total_players_logged_in": number_of_players
            #     }, broadcast=True)
            #
            #     if number_of_players == self.max_number_of_players:
            #         print("quiz_puzzle_start")
            #         puzzle.start()

    # LASER PUZZLE
    def on_laser_reset_puzzle(self):
        self.puzzles["laser"].reset_puzzle()

        socketio.emit("laser_puzzle_reset", "", broadcast=True, namespace="/test/{}".format(self.game_id))

    def send_to_players(self, event, message, broadcast):
        socketio.emit(event, message, broadcast=broadcast, namespace="/test/{}".format(self.game_id))
        print("event emitted", event, message)

    # def split_array(self, event, message):
    #     qa = message["TrickQuestionsArray"]
    #     aa = message["TrickAnswerArray"]
    #
    #     socketio.emit(event, {"TrickQuestionsArray": qa[:5], "TrickAnswerArray": aa[:5]}, namespace="/test/{}".format(self.game_id), room=self.players[0][1])
    #     socketio.emit(event, {"TrickQuestionsArray": qa[:10], "TrickAnswerArray": aa[:10]}, namespace="/test/{}".format(self.game_id), room=self.players[1][1])


    def on_test(self, message):
        print("test: " + message)

    def run(self):
        # Stage 1: users log in through the app
        self.all_players_logged_in.wait()

        # Stage 2: Mole is chosen and notified
        self.pick_mole()

        # Stage 3: Players solve puzzles
        self.game_completed.wait()


