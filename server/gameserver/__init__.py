from flask import Blueprint

from db_ma import socketio
from gameserver.mainserver import MainServer

gameserver_blueprint = Blueprint("gameserver", __name__)

mainserver = MainServer("/test")
socketio.on_namespace(mainserver)
