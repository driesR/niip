import _thread
import json
import socket
import threading
import time

import flask_socketio
import requests
from flask import request

from db_ma import socketio
from gameserver.game import Game
from gameserver.puzzles.LaserPuzzle import LaserPuzzle

API_ADDRESS = "http://localhost:5000/api"
# IP_ADDRESS = "192.168.16.175"
# IP_ADDRESS = "192.168.10.141"
IP_ADDRESS = "0.0.0.0"
PORT = 8082


class MainServer(threading.Thread, flask_socketio.Namespace):
    def __init__(self, namespace: str):
        flask_socketio.Namespace.__init__(self, namespace)
        threading.Thread.__init__(self, name="Gameserver")

        self.games = {}
        self.game_lock = threading.Lock()

        self.puzzles = {}
        self.puzzle_lock = threading.Lock()

    def recover_after_reset(self):
        print("recovery_after_reset")
        response = requests.get(API_ADDRESS + "/games")

        if response.status_code == 200:
            games = response.json()

            for game in games:
                game_id = game["game_id"]

                with self.game_lock:
                    if game_id not in self.games:
                        self.games[game_id] = Game("/test/{}".format(game_id), game_id)
                        socketio.on_namespace(self.games[game_id])

                        self.games[game_id].start()

                        print("restarted game {}".format(game_id))

        print("recovery_after_reset done")

    # PUZZLE PART
    def send_to_puzzle(self, sock: socket.socket, message):
        data = json.dumps(message).encode()

        data_send = False
        while not data_send:
            try:
                sock.send(data)
                data_send = True
                print("data send:", data)

            except socket.error as err:
                print("Cannot send data")
                time.sleep(2)

    def receive_from_puzzle(self, sock: socket.socket):
        data_received = False
        message = None

        while not data_received:
            try:
                data = sock.recv(1024)
                data_received = True
                print("data received:", data)
                message = json.loads(data.decode())

            except socket.error as err:
                print("Cannot receive data")
                time.sleep(2)

        return message

    def handle_puzzle_connection(self, conn: socket.socket, address):
        print("handle_puzzle_connection")
        # while True:
        msg = self.receive_from_puzzle(conn)

        if msg["type"] == "puzzle_registration":
            self.handle_puzzle_registration(msg, conn)

    def handle_puzzle_registration(self, msg: dict, conn: socket.socket):
        print("handle_puzzle_registration")
        puzzle_type = msg["message"]["puzzle_type"]

        # TODO: add API endpoint

        with self.puzzle_lock:
            if puzzle_type == "laser":
                self.puzzles[0] = LaserPuzzle(puzzle_type, conn)

        response = {
            "type": "puzzle_registration_ok"
        }

        self.send_to_puzzle(conn, response)
        print("handle_puzzle_registration done")

        self.on_add_puzzle_to_game(json.dumps({"game_id": 1, "puzzle_id": 0}))

    # GAME PART
    def on_create_game(self, data):
        print("on_create_game")
        message = json.loads(data)

        game_id = message["game_id"]

        with self.game_lock:
            if game_id not in self.games:
                self.games[game_id] = Game("/test/{}".format(game_id), game_id)
                socketio.on_namespace(self.games[game_id])

                self.games[game_id].start()

        print("on_create_game done")

    def on_add_puzzle_to_game(self, data):
        print("on_add_puzzle_to_game")
        message = json.loads(data)

        game_id = message["game_id"]
        puzzle_id = message["puzzle_id"]

        # TODO: add to api

        with self.game_lock, self.puzzle_lock:
            with self.games[game_id].puzzle_lock:
                self.games[game_id].add_puzzle("laser", self.puzzles[puzzle_id])

            print("on_add_puzzle_to_game done")

    # PLAYER PART
    def on_player_registration(self, data):
        print("on_player_registration")
        message = json.loads(data)

        print("Received player registration: {}".format(message))

        api_response = requests.get(API_ADDRESS + "/games?game_name={}".format(message["game"]["game_name"]))

        if api_response.status_code != 200:
            flask_socketio.emit("player_registration_failed", api_response.json())

        game_id = api_response.json()[0]["game_id"]

        event, details = self.games[game_id].add_player(message, request.sid)

        print("Emitting {}: {}".format(event, details))
        flask_socketio.emit(event, details)

        print("on_player_registration done")

    def run(self):
        self.recover_after_reset()

        puzzle_socket = socket.socket()
        puzzle_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        print(threading.get_ident())
        puzzle_socket.bind((IP_ADDRESS, PORT))

        puzzle_socket.listen(5)
        print("Listening on {}:{}".format(IP_ADDRESS, PORT))

        while True:
            conn, address = puzzle_socket.accept()
            print("Connection accepted")

            _thread.start_new_thread(self.handle_puzzle_connection, (conn, address))
