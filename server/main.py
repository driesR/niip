from flask import Flask
import time

from apiserver import api_blueprint
from db_ma import *
from gameserver import gameserver_blueprint, mainserver
from webserver import webserver_blueprint

app = Flask(__name__)

# WARNING: leave debug mode off, it gives problems with flask-socketio
app.config["DEBUG"] = False
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///test.db"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config['SECRET_KEY'] = 'gjr39dkjn344_!67#'

app.register_blueprint(webserver_blueprint, url_prefix="/")
app.register_blueprint(api_blueprint, url_prefix="/api")
app.register_blueprint(gameserver_blueprint)


def register_extensions(app):
    db.init_app(app)
    ma.init_app(app)
    bootstrap.init_app(app)
    socketio.init_app(app)

    db.create_all()
    time.sleep(3)
    mainserver.start()


app.app_context().push()

register_extensions(app)

if __name__ == "__main__":
    app.run(host="0.0.0.0")
