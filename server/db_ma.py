from flask_bootstrap import Bootstrap
from flask_marshmallow import Marshmallow
from flask_socketio import SocketIO
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
ma = Marshmallow()
bootstrap = Bootstrap()
socketio = SocketIO()
