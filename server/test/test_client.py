import socketio
import json
import time

socketio_client = socketio.Client()


def test_create_game():
    message = {
        "game_id": 1
    }

    socketio_client.emit("create_game", json.dumps(message), namespace="/test")

def test_login():
    message = {
        "game": {
            "game_name": "test",
            "password": "test"
        },
        "player": {
            "player_name": "azerty"
        }
    }
    socketio_client.emit("player_registration", json.dumps(message), namespace="/test")

@socketio_client.on("role assigned", namespace="/test")
def on_role_assigned(data):
    print(data)


def test():
    socketio_client.emit("test", "blablabla", namespace="/test/1")


if __name__ == '__main__':
    socketio_client.connect("http://localhost:5000", namespaces=["/test"])

    test_create_game()

    # socketio_client.connect("/test/1")
    # test()

    time.sleep(1)
    test_login()

    socketio_client.wait()
