package com.example.escapeit;

import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

//
// import com.seekcircle.SeekCircle;
//import com.seekcircle.sample.R;

import static com.example.escapeit.LoginActivity.UIHandler;

public class Explosion2 extends AppCompatActivity implements View.OnClickListener {

    TextView total;
    ProgressBar progressBar;
    int explosioncounter = 0;
    int totalplayers;
    int totalplayerslogedin = 0;

    int gameID;
    int playerID;
    private Handler progressBarHandler = new Handler();
    private Socket mSocket;
    private boolean startgame = false;

    private Button QRButton;
    static final int SCAN_REQUEST = 1;

    private SeekBar seekBar1;
    private SeekBar seekBar2;
    private SeekBar seekBar3;

    private Button button1;
    private Button button2;
    private Button button3;

    int round=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explosion2);


        //extract cookie data
        Intent i = getIntent();
        totalplayers = i.getIntExtra("totalplayers", 0);
        gameID = i.getIntExtra("gameid", 0);
        playerID = i.getIntExtra("playerid", 0);

        button1 = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);

        seekBar1 = findViewById(R.id.seekBar);
        seekBar2 = findViewById(R.id.seekBar2);
        seekBar3 = findViewById(R.id.seekBar3);
        seekBar1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) { return true; }
        });
        seekBar2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) { return true; }
        });
        seekBar3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) { return true; }
        });
        button2.setClickable(false);
        button3.setClickable(false);


        mSocket = SocketClass.getSocket();
        mSocket.on("laser_puzzle_verification", onPuzzleVerification);




    }

    static {
        UIHandler = new Handler(Looper.getMainLooper());
    }

    public static void runOnUI(Runnable runnable) {
        UIHandler.post(runnable);
    }

    private Emitter.Listener onPuzzleVerification = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            //LIKE IN THE QUIZ, SEND WHAT WAS RIGHT OR WRONG!!

            Intent data = new Intent();
            setResult(RESULT_OK, data);
            finish();

        }
    };


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonQuizNext2: //NEXT2 is last
                if((round) <3) {


                }else{

                    JSONObject QuizVerification = new JSONObject();
                    try {
                        JSONObject playerJson = new JSONObject();
                        playerJson.put("player_id", playerID);
                        playerJson.put("game_id", gameID);

                        QuizVerification.put("player", playerJson);
                        //QuizVerification.put("correct_answers", quizPoint);
                    } catch(JSONException e){ }
                    mSocket.emit("verify_quiz_puzzle2",QuizVerification.toString());

                    Intent data = new Intent();
                    setResult(RESULT_OK, data);
                    finish();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SCAN_REQUEST) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(getApplicationContext(),"QR of laser puzzle has been registered",Toast.LENGTH_SHORT).show();
                QRButton.setClickable(false);

                JSONObject ExplosionPuzzleJSON = new JSONObject();
                try {
                    JSONObject gameJson = new JSONObject();
                    gameJson.put("game_id", gameID);
                    gameJson.put("player_id", playerID);
                    gameJson.put("puzzle_name", "explosion");
                } catch (JSONException e) {
                }
                mSocket.connect();
                mSocket.emit("player_registration_puzzle", ExplosionPuzzleJSON.toString());

            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();

    }
}