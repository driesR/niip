package com.example.escapeit;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.example.escapeit.LoginActivity.UIHandler;

//import com.github.nkzawa.socketio.client.IO;
//import com.github.nkzawa.socketio.client.Socket;

public class Laser2 extends AppCompatActivity implements View.OnClickListener{


    TextView total;
    ProgressBar progressBar;
    int lasercounter=0;
    int totalplayerslogedin = 0;

    int totalplayers;
    int gameID;
    int playerID;
    private Handler progressBarHandler = new Handler();
    private Socket mSocket;
    private ArrayList<View> row2;
    private int row2counter = 0;

    static final int SCAN_REQUEST = 1;
    private Button ResetButton;

    Map<String, Integer> map = new HashMap<String, Integer>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laser2);
        //hashmap

        map.put("green", 0xFF00FF00);
        map.put("yellow", 0xFFFFFF00);
        map.put("white", 0xFFFFFFFF);
        map.put("cyan", 0xFF00FFFF);
        map.put("purple", 0xFFFF00FF);
        map.put("red", 0xFFFF0000);

        //extract intent data
        Intent in = getIntent();
        ArrayList<String> colors = in.getStringArrayListExtra("colors");

        ArrayList<View> row1 = new ArrayList<View>(); //just declare local not used global
        View view10 = findViewById(R.id.view10);row1.add(view10);
        View view11 = findViewById(R.id.view11);row1.add(view11);
        View view12 = findViewById(R.id.view12);row1.add(view12);
        View view13 = findViewById(R.id.view13);row1.add(view13);
        View view14 = findViewById(R.id.view14);row1.add(view14);
        View view15 = findViewById(R.id.view15);row1.add(view15);
        View view16 = findViewById(R.id.view16);row1.add(view16);
        View view17 = findViewById(R.id.view17);row1.add(view17);

        for (int i = 0; i < colors.size(); i++) {
            Log.d("coloararry",colors.get(i));
            row1.get(i).setBackgroundColor(map.get(colors.get(i)));
            Log.d("mapreceive","" +map.get(colors.get(i)));
        }

        row2 = new ArrayList<View>();
        View view20 = findViewById(R.id.view20);row2.add(view20);view20.setBackgroundColor(0xFF000000);
        View view21 = findViewById(R.id.view21);row2.add(view21);view21.setBackgroundColor(0xFF000000);
        View view22 = findViewById(R.id.view22);row2.add(view22);view22.setBackgroundColor(0xFF000000);
        View view23 = findViewById(R.id.view23);row2.add(view23);view23.setBackgroundColor(0xFF000000);
        View view24 = findViewById(R.id.view24);row2.add(view24);view24.setBackgroundColor(0xFF000000);
        View view25 = findViewById(R.id.view25);row2.add(view25);view25.setBackgroundColor(0xFF000000);
        View view26 = findViewById(R.id.view26);row2.add(view26);view26.setBackgroundColor(0xFF000000);
        View view27 = findViewById(R.id.view27);row2.add(view27);view27.setBackgroundColor(0xFF000000);


        ResetButton = (Button) findViewById(R.id.ButtonResetLaser); ResetButton.setOnClickListener(Laser2.this);


        mSocket = GameSocket.getInstance("");
        mSocket.on("laser_puzzle_color_listener", onNewColor);
        mSocket.on("laser_puzzle_reset", onPuzzleReset);
        mSocket.on("laser_puzzle_verification", onPuzzleVerification);
        mSocket.on("laser_puzzle_disconnect", onPuzzleDisconnect);


    }


    //NEW
    private Emitter.Listener onNewColor = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject data = (JSONObject) args[0];
            String color = "";

            try {
                color = data.getString("laser_color");
                if(row2counter < 8) {
                    Log.d("COLOR",color);
                    row2.get(row2counter).setBackgroundColor(map.get(color));
                    row2counter++;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
    //NEW
    private Emitter.Listener onPuzzleReset = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.d("yo","RESET LASER PUZZLE!!");
            for (int j = 0; j < row2.size(); j++) {
                row2.get(j).setBackgroundColor(0xFF000000);
            }
            row2counter= 0;
        }
    };

    private Emitter.Listener onPuzzleVerification = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            Log.d("yo","exited laser successfully");
            Intent data = new Intent();
            setResult(RESULT_OK, data);
            finish();

        }
    };
    private Emitter.Listener onPuzzleDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject data = (JSONObject) args[0];
            try {
                totalplayerslogedin = data.getInt("total_players_logged_in");
                Intent in = new Intent();
                in.putExtra("playerslogedin2", totalplayerslogedin);
                setResult(RESULT_OK, in);
                finish();

            }catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void onClick(View view)
    {
        switch (view.getId()) {
            case R.id.ButtonResetLaser:
                //mSocket.connect();
                mSocket.emit("laser_reset_puzzle"); //NEW
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
        mSocket.off("laser_puzzle_color_listener", onNewColor);
        mSocket.off("laser_puzzle_reset", onPuzzleReset);
        mSocket.off("laser_puzzle_verification", onPuzzleVerification);
        mSocket.off("laser_puzzle_disconnect", onPuzzleDisconnect);

    }
}
