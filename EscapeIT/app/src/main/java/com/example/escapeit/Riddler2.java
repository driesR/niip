package com.example.escapeit;

        import android.content.Intent;
        import android.net.Uri;
        import android.os.Handler;
        import android.os.Looper;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.Button;
        import android.widget.ProgressBar;
        import android.widget.TextView;
        import android.widget.Toast;

        import com.github.nkzawa.emitter.Emitter;
        import com.github.nkzawa.socketio.client.Socket;

        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        import java.util.ArrayList;
        import java.util.Locale;

        import static com.example.escapeit.LoginActivity.UIHandler;
        import static com.example.escapeit.Quiz.QUIZ1_REQUEST;

public class Riddler2 extends AppCompatActivity {


    TextView total;
    ProgressBar progressBar;
    int riddlercounter = 0;
    int totalplayers;
    int gameID;
    int playerID;
    private Handler progressBarHandler = new Handler();
    private Socket mSocket;

    private Button QRButton;
    static final int SCAN_REQUEST = 1;

    private String[] riddleArray = {"Bet you can't resist me on a hot summer day. I can make you burn and feel cold at the same time, I dare say. ",
            "Find the next clue near deep water. Man walks over, man walks under, in times of war he burns asunder?",
            "Where each turn finds you in a new mystery, a new adventure, or a new romance. "};

    private TextView Score;
    private TextView riddles;

    private int round = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_riddler2);
        Score = findViewById(R.id.textView3);
        riddles = findViewById(R.id.textView4);

        Intent in = getIntent();
        in.getIntExtra("round",round);

        Score.setText( "round: " +round + "/" + 3);
        riddles.setText(riddleArray[round]);

        mSocket = SocketClass.getSocket();
        mSocket.on("riddler_puzzle_start", onPuzzleVerification);
        mSocket.on("riddler_puzzle_solved", onNewRiddleSolved);

    }

    static {
        UIHandler = new Handler(Looper.getMainLooper());
    }

    public static void runOnUI(Runnable runnable) {
        UIHandler.post(runnable);
    }



    private Emitter.Listener onPuzzleVerification = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            Intent data = new Intent();
            setResult(RESULT_OK, data);
            finish();
        }
    };

    private Emitter.Listener onNewRiddleSolved = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            round++;
            Score.setText( round + "/" + 3);
            riddles.setText(riddleArray[round]);
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
        mSocket.off("riddler_puzzle_start", onPuzzleVerification);
        mSocket.off("riddler_puzzle_solved_riddle", onNewRiddleSolved);

    }
}