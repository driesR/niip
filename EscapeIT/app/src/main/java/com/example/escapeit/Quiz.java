package com.example.escapeit;

import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.example.escapeit.LoginActivity.UIHandler;

public class Quiz extends AppCompatActivity implements View.OnClickListener {


    TextView total;
    ProgressBar progressBar;
    int quizcounter = 0;
    int totalplayers;
    int totalplayerslogedin=0;
    boolean startgame;
    int gameID;
    int playerID;
    private Handler progressBarHandler = new Handler();
    private Socket mSocket;


    private ArrayList<String> TrickQuestionsArray = new ArrayList<String>();
    private ArrayList<String> TrickAnswerArray = new ArrayList<String>();
    //private List<String> TrickAnswerUserArray = new ArrayList<String>();

    private ArrayList<String> QuestionsArray = new ArrayList<String>();
    private ArrayList<String> AnswerArray1 = new ArrayList<String>();
    private ArrayList<String> AnswerArray2 = new ArrayList<String>();
    private ArrayList<String> AnswerArray3 = new ArrayList<String>();
    private ArrayList<String> AnswerArray4 = new ArrayList<String>();
    //private String[] AnswerArrayReceived = new String[5];


    ArrayList<String> TrickQuestionsArray2 = new ArrayList<String>();
    ArrayList<String> TrickAnswerArray2 = new ArrayList<String>();
    ArrayList<String> QuestionsArray2 = new ArrayList<String>();
    ArrayList<String> AnswerArray11 = new ArrayList<String>();
    ArrayList<String> AnswerArray12 = new ArrayList<String>();
    ArrayList<String> AnswerArray13 = new ArrayList<String>();
    ArrayList<String> AnswerArray14 = new ArrayList<String>();


    private Button QRButton;
    static final int SCAN_REQUEST = 1;
    private TextView logedin;
    static final int QUIZ1_REQUEST = 11;
    static final int QUIZ2_REQUEST = 12;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        setUpArray();

        Button googleMapsButton = findViewById(R.id.buttonGoogleQuiz); googleMapsButton.setOnClickListener(this);
        QRButton = findViewById(R.id.buttonQRQuiz); QRButton.setOnClickListener(this);

        //extract cookie data
        Intent i = getIntent();
        totalplayers = i.getIntExtra("totalplayers", 0);
        gameID = i.getIntExtra("gameid", 0);
        playerID = i.getIntExtra("playerid", 0);

        TextView total = findViewById(R.id.TotalQuiz);
        total.setText("0/"+totalplayers);

        //set progressbar
        total = findViewById(R.id.TotalQuiz);
        progressBar = findViewById(R.id.QuizProgressBar);
        progressBar.setProgress(0);
        progressBar.setMax(totalplayers);
        progressBar.setVisibility(View.VISIBLE);
        logedin = findViewById(R.id.textView9);
        logedin.setBackgroundColor(0xFFFF0000);


        mSocket = GameSocket.getInstance(Integer.toString(gameID));
        mSocket.on("player_quiz_login", onNewLogin);
        mSocket.on("quiz_puzzle_start", onPuzzleStart);

        //mSocket.emit("YO IK BEN TERUG") of mss via QR OPNIEUW LATEN REGISTEREN
    }

    static {
        UIHandler = new Handler(Looper.getMainLooper());
    }

    public static void runOnUI(Runnable runnable) {
        UIHandler.post(runnable);
    }

    private Emitter.Listener onNewLogin = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject data = (JSONObject) args[0];
            totalplayerslogedin = 0;
            try {
                totalplayerslogedin = data.getInt("total_players_logged_in");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            progressBarHandler.post(new Runnable() {
                public void run() {
                    TextView total = findViewById(R.id.TotalQuiz);
                    total.setText(totalplayerslogedin + "/" + totalplayers);
                    progressBar.setProgress(0);
                    progressBar.setMax(totalplayers);
                    progressBar.setProgress(totalplayerslogedin);
                }
            });
        }
    };
    private Emitter.Listener onPuzzleStart = new Emitter.Listener() {
        @Override
        public void call(Object... args) {


            //AT SERVER IF PUZZLE = STARTED; ANSWER 1ON1!
            JSONObject data = (JSONObject) args[0]; // ARRAYS VRAGEN
            int puzzlepart= 0;
            JSONArray arr;
            ArrayList<String> list;
            Log.d("yo","GOT QUIZ START EVENT");

            try {
               // totalplayerslogedin = data.getInt("total_players_logged_in");
                puzzlepart = data.getInt("quiz_part");
                boolean found = false;

                arr = new JSONArray(data.getJSONArray("player_finished_1")); list = new ArrayList<String>();
                for(int i=0; i< arr.length(); i++){
                    if(arr.getString(i).equals(playerID)){found =true;}
                }

                if(puzzlepart == 1 && !found){//&& if playerid != in this list: continue
                    arr = new JSONArray(data.getJSONArray("TrickQuestionsArray")); list = new ArrayList<String>();
                    for(int i = 0; i < arr.length(); i++){ list.add(arr.getString(i)); }
                    TrickQuestionsArray2 = list; // TrickQuestionsArray2 = TrickQuestionsArray;

                    arr = new JSONArray(data.getJSONArray("TrickAnswersArray")); list = new ArrayList<String>();
                    for(int i = 0; i < arr.length(); i++){ list.add(arr.getString(i)); }
                    TrickAnswerArray2 = list; //TrickAnswerArray2 = TrickAnswerArray;
                    openQuiz1();
                }
                else{
                    arr = new JSONArray(data.getJSONArray("QuestionsArray")); list = new ArrayList<String>();
                    for(int i = 0; i < arr.length(); i++){ list.add(arr.getString(i)); }
                    QuestionsArray2 = list; // QuestionsArray2 = QuestionsArray;

                    arr = new JSONArray(data.getJSONArray("AnswersArray1")); list = new ArrayList<String>();
                    for(int i = 0; i < arr.length(); i++){ list.add(arr.getString(i)); }
                    AnswerArray11 = list; //AnswerArray11 = AnswerArray1;

                    arr = new JSONArray(data.getJSONArray("AnswersArray2")); list = new ArrayList<String>();
                    for(int i = 0; i < arr.length(); i++){ list.add(arr.getString(i)); }
                    AnswerArray12 = list; //AnswerArray12 = AnswerArray2;

                    arr = new JSONArray(data.getJSONArray("AnswersArray3")); list = new ArrayList<String>();
                    for(int i = 0; i < arr.length(); i++){ list.add(arr.getString(i)); }
                    AnswerArray13 = list; //AnswerArray13 = AnswerArray3;

                    arr = new JSONArray(data.getJSONArray("AnswersArray4")); list = new ArrayList<String>();
                    for(int i = 0; i < arr.length(); i++){ list.add(arr.getString(i)); }
                    AnswerArray14 = list; //AnswerArray14 = AnswerArray4;

                    openQuiz2();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };

    private void openQuiz1(){

        Intent in = new Intent(Quiz.this, Quiz1.class);
        in.putStringArrayListExtra("trickquestions", TrickQuestionsArray2);
        in.putStringArrayListExtra("trickanswers", TrickAnswerArray2);
        in.putExtra("playerid", playerID);
        in.putExtra("gameid", gameID);
        startActivityForResult(in, QUIZ1_REQUEST);
    }
    private void openQuiz2(){
        Intent in = new Intent(Quiz.this, Quiz2.class);
        in.putStringArrayListExtra("questions", QuestionsArray2);
        in.putStringArrayListExtra("answers1", AnswerArray11); //ARRAY1 is TRUE
        in.putStringArrayListExtra("answers2", AnswerArray12); //HARDCODED OR PLAYER's
        in.putStringArrayListExtra("answers3", AnswerArray13); //HARDCODED OR PLAYER's
        in.putStringArrayListExtra("answers4", AnswerArray14); //HARDCODED OR PLAYER's
        in.putExtra("playerid", playerID);
        in.putExtra("gameid", gameID);
        startActivityForResult(in, QUIZ2_REQUEST);
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonGoogleQuiz:

                Intent in = new Intent(Quiz.this, Quiz1.class);
                in.putStringArrayListExtra("trickquestions", TrickQuestionsArray);
                in.putStringArrayListExtra("trickanswers", TrickAnswerArray);
                in.putExtra("playerid", playerID);
                in.putExtra("gameid", gameID);
                startActivityForResult(in, QUIZ1_REQUEST);
                break;
                /*
                double destinationLatitude = 50.930103;
                double destinationLongitude = 5.338349;
                String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%f,%f (%s)", destinationLatitude, destinationLongitude, "LASERLOCATION");
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
                break;
                */
            case R.id.buttonQRQuiz:

                Intent in2 = new Intent(Quiz.this, Quiz2.class);
                in2.putStringArrayListExtra("questions", QuestionsArray);
                in2.putStringArrayListExtra("answers1", AnswerArray1); //ARRAY1 is TRUE
                in2.putStringArrayListExtra("answers2", AnswerArray2); //HARDCODED OR PLAYER's
                in2.putStringArrayListExtra("answers3", AnswerArray3); //HARDCODED OR PLAYER's
                in2.putStringArrayListExtra("answers4", AnswerArray4); //HARDCODED OR PLAYER's
                in2.putExtra("playerid", playerID);
                in2.putExtra("gameid", gameID);
                startActivityForResult(in2, QUIZ2_REQUEST);
                break;


                /* //TO SKIP JUST QR SCAN
                //Intent i=new Intent(this,QR.class);
                //startActivityForResult(i,SCAN_REQUEST);

                JSONObject QuizPuzzleJSON = new JSONObject();
                try {
                    //JSONObject gameJson = new JSONObject();
                    QuizPuzzleJSON.put("game_id", gameID);
                    QuizPuzzleJSON.put("player_id", playerID);
                    QuizPuzzleJSON.put("puzzle_name", "quiz");
                    //LaserPuzzleJSON.put("game", gameJson);
                } catch(JSONException e){ }
                //mSocket.connect();

                mSocket.emit("player_registration_puzzle", QuizPuzzleJSON.toString());
                logedin.setBackgroundColor(0xFF00FF00);
                */


        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SCAN_REQUEST) {
            if (resultCode == RESULT_OK) {
                QRButton.setClickable(false);
                JSONObject QuizPuzzleJSON = new JSONObject();
                try {
                    QuizPuzzleJSON.put("game_id", gameID);
                    QuizPuzzleJSON.put("player_id", playerID);
                    QuizPuzzleJSON.put("puzzle_name", "quiz");
                } catch (JSONException e) {
                }
                mSocket.connect();
                mSocket.emit("player_registration_puzzle", QuizPuzzleJSON.toString());
                logedin.setBackgroundColor(0xFF00FF00);
            }
        }
        if (requestCode == QUIZ1_REQUEST) {
            if (resultCode == RESULT_OK) {
            }
        }
        if (requestCode == QUIZ2_REQUEST) {
            if (resultCode == RESULT_OK) {
                Intent D = new Intent();
                //PUSH ALONG POINTS!!! // TRY TO HANDLE VIA ONRESUME
                setResult(RESULT_OK, D);
                finish();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
        mSocket.off("player_quiz_login", onNewLogin);
        mSocket.off("quiz_puzzle_start", onPuzzleStart);

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void setUpArray(){

        TrickQuestionsArray.add("What was Justin Bieber's first top 10 hit?");
        TrickQuestionsArray.add("What country has the third biggest military?");
        TrickQuestionsArray.add("In which country took the D-Day offensive place?");
        TrickQuestionsArray.add("Who was the EuroVisionSong winner 2019?");
        TrickQuestionsArray.add("Who was the first American in space?");
        TrickQuestionsArray.add("What is the KKK");
        TrickQuestionsArray.add("Who is the drummer of Metallica?");
        TrickQuestionsArray.add("In which country did Shakespeare s Hamlet live?");
        TrickQuestionsArray.add("Which Egyptian woman was considered as the god of agriculture? She married her brother and together they had a son: Horus.");
        TrickQuestionsArray.add("Who said \"I think therefore I am\"?");

        TrickAnswerArray.add("One Time");
        TrickAnswerArray.add("China");
        TrickAnswerArray.add("France");
        TrickAnswerArray.add("The Netherlands");
        TrickAnswerArray.add("Alan Shepard");
        TrickAnswerArray.add("Ku Klux Klan");
        TrickAnswerArray.add("Lars Ulrich");
        TrickAnswerArray.add("Denmark");
        TrickAnswerArray.add("ISIS");
        TrickAnswerArray.add("Rene Descartes");

        QuestionsArray.add("How many presidents have there been in the USA");
        QuestionsArray.add("How long did WW2 last?");
        QuestionsArray.add("What is the lightest existing metal?");
        QuestionsArray.add("What were the first words ever spoken on the telephone by Alexander Graham Bell");
        QuestionsArray.add("Which plant does the Canadian flag contain?");
        QuestionsArray.add("how much is the squareroot of 44100");
        QuestionsArray.add("Fill in the quote of Napoleon Bonaparte. An army marches on its ...");
        QuestionsArray.add("Which French king was called the Sun King?");
        QuestionsArray.add("What is the organ that is affected when one is suffering from hepatitis?");
        QuestionsArray.add("What was in England the northern frontier of the Roman Empire?");

        AnswerArray1.add("45");
        AnswerArray1.add("6years");
        AnswerArray1.add("Aluminium");
        AnswerArray1.add("Mr. Watson, come here, I want to see you.");
        AnswerArray1.add("Maple");
        AnswerArray1.add("210");
        AnswerArray1.add("Stomach");
        AnswerArray1.add("Louis XIV");
        AnswerArray1.add("Liver");
        AnswerArray1.add("Hadrian's wall");

        AnswerArray2.add("40");
        AnswerArray2.add("5years");
        AnswerArray2.add("Silver");
        AnswerArray2.add("It works!!");
        AnswerArray2.add("Oak");
        AnswerArray2.add("201");
        AnswerArray2.add("Feet");
        AnswerArray2.add("Alexander the great");
        AnswerArray2.add("Kidneys");
        AnswerArray2.add("Chinese wall");

        AnswerArray3.add("588");
        AnswerArray3.add("4Years");
        AnswerArray3.add("Tin");
        AnswerArray3.add("Roger");
        AnswerArray3.add("Willow");
        AnswerArray3.add("404");
        AnswerArray3.add("Mind");
        AnswerArray3.add("Caesar");
        AnswerArray3.add("Skin");
        AnswerArray3.add("Heralds wall");

        AnswerArray4.add("58");
        AnswerArray4.add("25years");
        AnswerArray4.add("Copper");
        AnswerArray4.add("Does it work?");
        AnswerArray4.add("Yew");
        AnswerArray4.add("200");
        AnswerArray4.add("Toes");
        AnswerArray4.add("henry");
        AnswerArray4.add("Eyes");
        AnswerArray4.add("Zack Efron");
        AnswerArray4.add("Skin");


    }
}