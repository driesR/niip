package com.example.escapeit;

import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import static com.example.escapeit.LoginActivity.UIHandler;

public class Quiz2 extends AppCompatActivity implements View.OnClickListener {


    int totalplayerslogedin=0;
    int gameID;
    int playerID;
    private Handler progressBarHandler = new Handler();
    private Socket mSocket;

    static final int SCAN_REQUEST = 1;

    private int trickAnswers = 0;

    private TextView AnswerView2;
    private TextView AnswerView3;
    private TextView AnswerView4;
    private TextView AnswerView5;
    private TextView QuestionView;
    private Button nextButton3;
    private RadioGroup radioGroup;

    private List<String> QuestionsArray = new ArrayList<String>();
    private List<String> AnswerArray1 = new ArrayList<String>();
    private List<String> AnswerArray2 = new ArrayList<String>();
    private List<String> AnswerArray3 = new ArrayList<String>();
    private List<String> AnswerArray4 = new ArrayList<String>();
    //private String[] AnswerArrayReceived = new String[5];

    Random rand = new Random();

    int randnumb;
    int max = 4;
    int min = 1;
    int quizPoint =0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quizquestions2);
        nextButton3 = findViewById(R.id.buttonQuizNext2); nextButton3.setOnClickListener(this);
        AnswerView2 = findViewById(R.id.AnswerView2);
        AnswerView3 = findViewById(R.id.AnswerView3);
        AnswerView4 = findViewById(R.id.AnswerView4);
        AnswerView5 = findViewById(R.id.AnswerView5);
        QuestionView = findViewById(R.id.questionView);
        radioGroup = findViewById(R.id.radiogroup);
        //set initial questions

        Intent in = getIntent();
        QuestionsArray = in.getStringArrayListExtra("questions");
        AnswerArray1 = in.getStringArrayListExtra("answers1");
        AnswerArray2 = in.getStringArrayListExtra("answers2");
        AnswerArray3 = in.getStringArrayListExtra("answers3");
        AnswerArray4 = in.getStringArrayListExtra("answers4");
        gameID = in.getIntExtra("gameid",0);
        playerID = in.getIntExtra("gameid",0);



        QuestionView.setText(QuestionsArray.get(trickAnswers));
        AnswerView2.setText(AnswerArray1.get(trickAnswers));
        AnswerView3.setText(AnswerArray2.get(trickAnswers));
        AnswerView4.setText(AnswerArray3.get(trickAnswers));
        AnswerView5.setText(AnswerArray4.get(trickAnswers));
        randnumb = 1;
        trickAnswers++;


        mSocket = GameSocket.getInstance("");
       // mSocket.on("quiz_puzzle_verification_2", onPuzzleVerification);
        //mSocket.on("quiz_puzzle_disconnect", onPuzzleDisconnect);


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonQuizNext2: //NEXT2 is last
                if((trickAnswers) < 5) {
                    //setContentView(R.layout.activity_quizquestions2);
                    //Log.d("tag","yolo");

                    //if prev randnumb = indicated by radio button selected => 1 correct!
                    int selectedId = radioGroup.getCheckedRadioButtonId();
                    View radioButton = radioGroup.findViewById(selectedId);
                    int idx = radioGroup.indexOfChild(radioButton);
                    if(idx+1 == randnumb){
                        quizPoint++;
                    }

                    //set question
                    QuestionView.setText(QuestionsArray.get(trickAnswers));
                    //set answers "random like latin square" (normally 4*4 permutations possible)
                    randnumb = rand.nextInt(max - min + 1) + min;
                    if(randnumb==1){
                        AnswerView2.setText(AnswerArray1.get(trickAnswers));
                        AnswerView3.setText(AnswerArray2.get(trickAnswers));
                        AnswerView4.setText(AnswerArray3.get(trickAnswers));
                        AnswerView5.setText(AnswerArray4.get(trickAnswers));
                    }else if(randnumb == 2){
                        AnswerView2.setText(AnswerArray4.get(trickAnswers));
                        AnswerView3.setText(AnswerArray1.get(trickAnswers));
                        AnswerView4.setText(AnswerArray2.get(trickAnswers));
                        AnswerView5.setText(AnswerArray3.get(trickAnswers));
                    }else if(randnumb == 3){
                        AnswerView2.setText(AnswerArray3.get(trickAnswers));
                        AnswerView3.setText(AnswerArray4.get(trickAnswers));
                        AnswerView4.setText(AnswerArray1.get(trickAnswers));
                        AnswerView5.setText(AnswerArray2.get(trickAnswers));
                    }else {
                        AnswerView2.setText(AnswerArray2.get(trickAnswers));
                        AnswerView3.setText(AnswerArray3.get(trickAnswers));
                        AnswerView4.setText(AnswerArray4.get(trickAnswers));
                        AnswerView5.setText(AnswerArray1.get(trickAnswers));
                    }
                    radioGroup.clearCheck();
                    trickAnswers++;
                }else{
                    int selectedId = radioGroup.getCheckedRadioButtonId();
                    View radioButton = radioGroup.findViewById(selectedId);
                    int idx = radioGroup.indexOfChild(radioButton);
                    if(idx+1 == randnumb){
                        quizPoint++;
                    }

                    JSONObject QuizVerification = new JSONObject();
                    try {
                        JSONObject playerJson = new JSONObject();
                        playerJson.put("player_id", playerID);
                        playerJson.put("game_id", gameID);

                        QuizVerification.put("player", playerJson);
                        QuizVerification.put("correct_answers", quizPoint);
                    } catch(JSONException e){ }
                    mSocket.emit("verify_quiz_puzzle2",QuizVerification.toString());

                    Intent data = new Intent();
                    setResult(RESULT_OK, data);
                    finish();
                }
                    break;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
        //mSocket.off("quiz_puzzle_verification", onPuzzleVerification);
       // mSocket.off("quiz_puzzle_disconnect", onPuzzleDisconnect);

    }
}