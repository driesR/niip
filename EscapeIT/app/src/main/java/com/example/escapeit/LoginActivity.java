package com.example.escapeit;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;




public class LoginActivity extends AppCompatActivity {
    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;
    private EditText mGameView;
    private Socket mSocket;
    public static Handler UIHandler;
    public Socket gameSocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mEmailView = findViewById(R.id.name);
        mGameView = findViewById(R.id.game);
        mPasswordView = findViewById(R.id.password);

        mSocket = SocketClass.getSocket();
        mSocket.on("player_registration_successful", onSuccessLogin);
        mSocket.on("player_registration_failed", onFailLogin);


        Button mEmailSignInButton = findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });


        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
    }

    private void attemptLogin() {
        boolean correct = false;

        //String URL = "http://192.168.16.175:5000/api/players" ;
        String name = mEmailView.getText().toString(); //USERNAME
        String password = mPasswordView.getText().toString(); //PASSWORD
        String game = mGameView.getText().toString();


        if(!(name.equals("") || password.equals("") || game.equals(""))){
            String response = "";
            JSONObject loginJson = new JSONObject();
            try {
                JSONObject gameJson = new JSONObject();
                gameJson.put("game_name", game);
                gameJson.put("password", password);

                JSONObject playerJson = new JSONObject();
                playerJson.put("player_name", name);

                loginJson.put("player", playerJson);
                loginJson.put("game", gameJson);
            } catch(JSONException e){ }
            mSocket.connect();
            mSocket.emit("player_registration", loginJson.toString());
            //Log.d("myTag", response );

            /*
            try {
              response = APICalls.HttpPostCode(URL, loginJson);
            } catch (IOException e) {
                e.printStackTrace();
            }
            */

        }
    }

    static{
        UIHandler = new Handler(Looper.getMainLooper());
    }
    public static void runOnUI(Runnable runnable) {
        UIHandler.post(runnable);
    }

    private Emitter.Listener onFailLogin = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            LoginActivity.runOnUI(new Runnable() {
                public void run() {
                    Toast.makeText(LoginActivity.this, "LOGIN FAILED", Toast.LENGTH_SHORT).show();
                }
            });
        }
    };


    private Emitter.Listener onSuccessLogin = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject data = (JSONObject) args[0];
            int playerID = 0;
            int gameID = 0;
            int totalplayers = 0;
            try {
                playerID = data.getInt("player_id");
                gameID = data.getInt("game_id");
                totalplayers = data.getInt("max_number_of_players");

                ///§!!!!!! GET WHICH GAMES COMPLETED!!!!///

            } catch (JSONException e) {
                e.printStackTrace();
            }

            Socket msocket = GameSocket.getInstance(Integer.toString(gameID));
            msocket.connect();



            //SAVE TO RELOGIN
            String name = mEmailView.getText().toString(); //USERNAME
            String password = mPasswordView.getText().toString(); //PASSWORD
            String game = mGameView.getText().toString();

            SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = app_preferences.edit();
            editor.putString("name", mEmailView.getText().toString());
            editor.putString("password", mPasswordView.getText().toString());
            editor.putString("game", mGameView.getText().toString());
            editor.commit();




            openMain(playerID,gameID,totalplayers);
        }
    };
    private void openMain(int playerID, int gameID, int totalplayers){
        Intent activityIntent;
        activityIntent = new Intent(this, MainActivity.class);
        activityIntent.putExtra("playerid", playerID);
        activityIntent.putExtra("gameid", gameID);
        activityIntent.putExtra("totalplayers", totalplayers);
        startActivity(activityIntent);
        finish();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        mSocket.disconnect();
        mSocket.off("player_registration_successful", onSuccessLogin);
        mSocket.off("player_registration_failed", onFailLogin);

    }

    @Override
    public void onResume(){
        super.onResume();

        SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        mEmailView.setText(app_preferences.getString("name", ""));
        mPasswordView.setText(app_preferences.getString("password", ""));
        mGameView.setText(app_preferences.getString("game", ""));

    }

}

