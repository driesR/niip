package com.example.escapeit;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QR extends Activity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView zXingScannerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);
        zXingScannerView = new ZXingScannerView(getApplicationContext());
        setContentView(zXingScannerView);
        zXingScannerView.setResultHandler(this);
        zXingScannerView.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        zXingScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result result) {
        //Toast.makeText(getApplicationContext(),getCallingActivity().getClassName(),Toast.LENGTH_SHORT).show();
        zXingScannerView.resumeCameraPreview(this);

        Log.d("CLASSNAME", getCallingActivity().getClassName());
        Log.d("RESULT", "com.example.escapeit."+ result.getText());

        if(("com.example.escapeit."+ result.getText()).equals(getCallingActivity().getClassName())){
            Toast.makeText(getApplicationContext(),"yes",Toast.LENGTH_SHORT).show();
            Intent data = new Intent();
            setResult(RESULT_OK, data);
            finish();
        }
    }

}
