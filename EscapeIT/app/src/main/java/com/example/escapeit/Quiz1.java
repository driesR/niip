package com.example.escapeit;

import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.example.escapeit.LoginActivity.UIHandler;

public class Quiz1 extends AppCompatActivity implements View.OnClickListener {



    int gameID;
    int playerID;
    private Handler progressBarHandler = new Handler();
    private Socket mSocket;

    private ArrayList<String> TrickQuestionsArray = new ArrayList<String>();
    private ArrayList<String>  TrickAnswerArray = new ArrayList<String>();
    //private List<String> TrickAnswerUserArray = new ArrayList<String>();
    JSONArray array = new JSONArray();


    private int trickAnswers = 0;
    private TextView trickAnswerView;
    private TextView trickQuestionView;
    private EditText editTextAnswer;

    private Button nextButton1;
    private TextView startingNextView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quizquestions);
        trickAnswerView = findViewById(R.id.quizTrickAnswerView);
        trickQuestionView = findViewById(R.id.quizTrickQuestionView);
        editTextAnswer = findViewById(R.id.editText);
        nextButton1 = findViewById(R.id.buttonQuizNext1); nextButton1.setOnClickListener(this);
        startingNextView = findViewById(R.id.startingNextView);

        startingNextView.setVisibility(View.INVISIBLE);
        nextButton1.setVisibility(View.VISIBLE);

        Intent in = getIntent();
        TrickQuestionsArray = in.getStringArrayListExtra("trickquestions");
        TrickAnswerArray = in.getStringArrayListExtra("trickanswers");
        gameID = in.getIntExtra("gameid",0);
        playerID = in.getIntExtra("gameid",0);

        trickQuestionView.setText(TrickQuestionsArray.get(trickAnswers));
        trickAnswerView.setText(TrickAnswerArray.get(trickAnswers));
        trickAnswers++;

        mSocket = GameSocket.getInstance("");
        //mSocket.on("quiz_puzzle_verification_1", onPuzzleVerification);
        //mSocket.on("quiz_puzzle_disconnect", onPuzzleDisconnect);



    }


    //UITWERKEN
    private Emitter.Listener onPuzzleVerification = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            //Emit playerID; finishedPart1;
            Intent data = new Intent();
            setResult(RESULT_OK, data);
            finish();
        }
    };

    /*
    //KOPIEER VOLGENDE KLASSE!! a add in main quiz klasse call if this class is finished(en call dan direct second if true)
    private Emitter.Listener onPuzzleDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
           JSONObject data = (JSONObject) args[0];
            try {
                totalplayerslogedin = data.getInt("total_players_logged_in");
                Intent in = new Intent();
                in.putExtra("playerslogedin2", totalplayerslogedin);
                setResult(RESULT_OK, in);
                finish();

            }catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };*/

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonQuizNext1:
                if(trickAnswers < 5) {
                    trickQuestionView.setText(TrickQuestionsArray.get(trickAnswers));
                    trickAnswerView.setText(TrickAnswerArray.get(trickAnswers));
                    array.put(editTextAnswer.getText());
                    editTextAnswer.setText("");
                    trickAnswers++;

                }
                else{
                    startingNextView.setVisibility(View.VISIBLE);
                    nextButton1.setVisibility(View.INVISIBLE);
                    //FINISH EMIT!!!

                    JSONObject QuizVerification = new JSONObject();
                    try {
                        JSONObject playerJson = new JSONObject();
                        playerJson.put("player_id", playerID);
                        playerJson.put("game_id", gameID);

                        QuizVerification.put("player", playerJson);
                        QuizVerification.put("trick_answer_array", array);
                    } catch(JSONException e){ }
                    mSocket.emit("verify_quiz_puzzle1",QuizVerification.toString());
                }
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
        //mSocket.off("quiz_puzzle_verification", onPuzzleVerification);
        //mSocket.off("quiz_puzzle_disconnect", onPuzzleDisconnect);

    }
}