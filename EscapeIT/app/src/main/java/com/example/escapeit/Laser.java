package com.example.escapeit;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import static com.example.escapeit.LoginActivity.UIHandler;

//import com.github.nkzawa.socketio.client.IO;
//import com.github.nkzawa.socketio.client.Socket;

public class Laser extends AppCompatActivity implements View.OnClickListener{


    //TextView total;
    ProgressBar progressBar;
    int lasercounter=0;
    int totalplayerslogedin = 0;

    int totalplayers;
    int gameID;
    int playerID;
    private Handler progressBarHandler = new Handler();

    //private Handler textViewHandler = new Handler();

    private Socket mSocket;

    private Button QRButton;
    static final int SCAN_REQUEST = 1;

    private TextView logedin;

    static final int LASER2_REQUEST= 11;

    String totalplayerslogedin2;

    private ArrayList<String>  ColorsArray = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laser);

        ColorsArray.add("green");
        ColorsArray.add("yellow");
        ColorsArray.add("green");
        ColorsArray.add("white");
        ColorsArray.add("green");
        ColorsArray.add("purple");
        ColorsArray.add("red");
        ColorsArray.add("cyan");

        Button googleMapsButton = (Button) findViewById(R.id.buttonGoogleLaser); googleMapsButton.setOnClickListener(this);
        QRButton = (Button) findViewById(R.id.buttonQRLaser); QRButton.setOnClickListener(this);

        //extract cookie data
        Intent i = getIntent();
        totalplayers = i.getIntExtra("totalplayers",0);
        gameID = i.getIntExtra("gameid",0);
        playerID = i.getIntExtra("playerid",0);

        TextView total = findViewById(R.id.TotalLaser);
        total.setText("0/"+totalplayers);

        //set progressbar
        progressBar = findViewById(R.id.LaserProgressBar);
        progressBar.setProgress(0);
        progressBar.setMax(totalplayers);
        progressBar.setVisibility(View.VISIBLE);

        logedin = findViewById(R.id.textView5);
        logedin.setBackgroundColor(0xFFFF0000);


        //mSocket = SocketClass.getSocket();
        mSocket = GameSocket.getInstance(Integer.toString(gameID));
        mSocket.on("player_laser_login", onNewLogin);
        mSocket.on("laser_puzzle_start", onPuzzleStart);

        //openLaser2(ColorsArray); // COMMENT TO GET BACK IN ORDER

    }

    static{
        UIHandler = new Handler(Looper.getMainLooper());
    }
    public static void runOnUI(Runnable runnable) {
        UIHandler.post(runnable);
    }

    private Emitter.Listener onNewLogin = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject data = (JSONObject) args[0];
            totalplayerslogedin = 0;
            try {
                totalplayerslogedin = data.getInt("total_players_logged_in");
            } catch (JSONException e) {
                e.printStackTrace();
            }


                progressBarHandler.post(new Runnable() {
                    public void run() {
                        TextView total = findViewById(R.id.TotalLaser);
                        total.setText(totalplayerslogedin + "/" + totalplayers);
                        progressBar.setProgress(0);
                        progressBar.setMax(totalplayers);
                        progressBar.setProgress(totalplayerslogedin);
                    }
                });
        }
    };
    private Emitter.Listener onPuzzleStart = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            try {
                JSONObject data = (JSONObject) args[0];
                JSONArray colors;

                colors = data.getJSONArray("colors");

                ArrayList<String> listdata = new ArrayList<String>();
                JSONArray jArray = (JSONArray)colors;
                if (jArray != null) {
                    for (int i=0;i<jArray.length();i++){
                        listdata.add(jArray.getString(i));
                    }
                }

                openLaser2(listdata);



            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private void openLaser2(ArrayList<String> listdata){
        Intent i = new Intent(Laser.this, Laser2.class);

        i.putStringArrayListExtra("colors", listdata);
        startActivityForResult(i, LASER2_REQUEST);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //WHEN I COME BACK IN THIS ACTIVITY DO NOTHING LET HIM SCAN AGAIN TO REENTER!!

    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId()) {
            case R.id.buttonGoogleLaser:
                double destinationLatitude = 50.930103;
                double destinationLongitude = 5.338349;
                String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%f,%f (%s)", destinationLatitude, destinationLongitude, "LASERLOCATION");
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
                break;
            case R.id.buttonQRLaser:
                Intent i=new Intent(this,QR.class);
                startActivityForResult(i,SCAN_REQUEST);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SCAN_REQUEST) {
            if (resultCode == RESULT_OK) {
                //CALL SIGN IN LASERGAME!!!!!
                Toast.makeText(getApplicationContext(),"QR of laser puzzle has been registered",Toast.LENGTH_SHORT).show();
                QRButton.setClickable(false);

                JSONObject LaserPuzzleJSON = new JSONObject();
                try {
                    LaserPuzzleJSON.put("game_id", gameID);
                    LaserPuzzleJSON.put("player_id", playerID);
                    LaserPuzzleJSON.put("puzzle_name", "laser");
                } catch(JSONException e){ }
                //mSocket.connect();
                mSocket.emit("player_registration_puzzle", LaserPuzzleJSON.toString());
                logedin.setBackgroundColor(0xFF00FF00);

            }
        }
        if (requestCode == LASER2_REQUEST) {
            if (resultCode == RESULT_OK) {
                Intent D = new Intent();
                setResult(RESULT_OK, D);
                finish();
            }
            else{

                totalplayerslogedin2 = data.getStringExtra("playerslogedin2");
                progressBarHandler.post(new Runnable() {
                    public void run() {
                        TextView total = findViewById(R.id.TotalLaser);
                        total.setText(totalplayerslogedin2 + "/" + totalplayers);
                        progressBar.setProgress(0);
                        progressBar.setMax(totalplayers);
                        progressBar.setProgress(totalplayerslogedin);
                    }
                });
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
        mSocket.off("player_laser_login", onNewLogin);
        mSocket.off("laser_puzzle_start", onPuzzleStart);

    }
}
