package com.example.escapeit;

import android.app.Application;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;


public class SocketClass  {

    private static Socket mSocket;
    static {
        try{
            mSocket = IO.socket("http://192.168.16.175:5000/test");
        }catch(URISyntaxException e){throw new RuntimeException(e);}
    }


    public static Socket getSocket(){
        return mSocket;
    }
}
