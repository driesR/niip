package com.example.escapeit;

import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.example.escapeit.LoginActivity.UIHandler;

public class Explosion  extends AppCompatActivity implements View.OnClickListener {


    TextView total;
    ProgressBar progressBar;
    int explosioncounter = 0;
    int totalplayers;
    int totalplayerslogedin = 0;

    int gameID;
    int playerID;
    private Handler progressBarHandler = new Handler();
    private Socket mSocket;

    private Button QRButton;
    static final int SCAN_REQUEST = 1;

    private List<String> minArray = new ArrayList<String>();
    private List<String> maxArray = new ArrayList<String>();

    static final int EXPLOSION_REQUEST = 11;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explosion);

        Button googleMapsButton = (Button) findViewById(R.id.buttonGoogleExplosion);
        googleMapsButton.setOnClickListener(this);
        QRButton = (Button) findViewById(R.id.buttonQRExplosion);
        QRButton.setOnClickListener(this);

        //extract cookie data
        Intent i = getIntent();
        totalplayers = i.getIntExtra("totalplayers", 0);
        gameID = i.getIntExtra("gameid", 0);
        playerID = i.getIntExtra("playerid", 0);

        //set progressbar
        total = findViewById(R.id.TotalExplosion);
        progressBar = findViewById(R.id.ExplosionProgressBar);
        progressBar.setProgress(0);
        progressBar.setMax(totalplayers);
        progressBar.setVisibility(View.VISIBLE);


        mSocket = SocketClass.getSocket();
        mSocket.on("player_explosion_login", onNewLogin);
        mSocket.on("player_explosion_start", onStart);
        mSocket.on("laser_puzzle_verification", onPuzzleVerification);

        openExplosion2();
    }

    static {
        UIHandler = new Handler(Looper.getMainLooper());
    }

    public static void runOnUI(Runnable runnable) {
        UIHandler.post(runnable);
    }

    private Emitter.Listener onNewLogin = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject data = (JSONObject) args[0];
            try {
                totalplayerslogedin = data.getInt("total_players_logged_in");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (totalplayerslogedin != totalplayers) {
                total.setText(totalplayerslogedin + "/" + totalplayers);
                progressBarHandler.post(new Runnable() {
                    public void run() {
                        progressBar.setProgress(0);
                        progressBar.setMax(totalplayers);
                        progressBar.setProgress(explosioncounter);
                    }
                });
            } else {
                //if complete open new activity with the game&layout
            }
        }
    };

    private Emitter.Listener onStart = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject data = (JSONObject) args[0];

            JSONArray arr;
            ArrayList<String> list;

            try {
                arr = new JSONArray(data.getJSONArray("min_array"));
                list = new ArrayList<String>();
                for (int i = 0; i < arr.length(); i++) {
                    list.add(arr.getString(i));
                }
                minArray = list; // TrickQuestionsArray2 = TrickQuestionsArray;

                arr = new JSONArray(data.getJSONArray("max_array"));
                list = new ArrayList<String>();
                for (int i = 0; i < arr.length(); i++) {
                    list.add(arr.getString(i)); //list.add(arr.getJSONObject(i).getString("name")); // SHOULD IT BE THIS ONE AT QUIZ???
                }
                maxArray = list; //TrickAnswerArray2 = TrickAnswerArray;
                openExplosion2();


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };

    private void openExplosion2(){

        Intent in = new Intent(Explosion.this, Explosion2.class);
        //in.putStringArrayListExtra("trickquestions", TrickQuestionsArray2); //STILL NEED TO PUSH ARRAYS of range(s)
        //in.putStringArrayListExtra("trickanswers", TrickAnswerArray2);
        in.putExtra("playerid", playerID);
        in.putExtra("gameid", gameID);
        startActivityForResult(in, EXPLOSION_REQUEST);
    }

    private Emitter.Listener onPuzzleVerification = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            Intent data = new Intent();
            setResult(RESULT_OK, data);
            finish();

        }
    };


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonGoogleExplosion:
                double destinationLatitude = 50.930103;
                double destinationLongitude = 5.338349;
                String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%f,%f (%s)", destinationLatitude, destinationLongitude, "EXPLOSIONLOCATION");
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
                break;
            case R.id.buttonQRExplosion:

                Intent in = new Intent(Explosion.this, Explosion2.class);
               // in.putExtra("playerid", playerID);
               // in.putExtra("gameid", gameID);
                startActivityForResult(in, EXPLOSION_REQUEST);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SCAN_REQUEST) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(getApplicationContext(),"QR of laser puzzle has been registered",Toast.LENGTH_SHORT).show();
                QRButton.setClickable(false);

                JSONObject ExplosionPuzzleJSON = new JSONObject();
                try {
                    ExplosionPuzzleJSON.put("game_id", gameID);
                    ExplosionPuzzleJSON.put("player_id", playerID);
                    ExplosionPuzzleJSON.put("puzzle_name", "explosion");
                } catch (JSONException e) {
                }
                mSocket.connect();
                mSocket.emit("player_registration_puzzle", ExplosionPuzzleJSON.toString());

            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
        mSocket.off("player_explosion_login", onNewLogin);

    }
}