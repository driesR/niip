package com.example.escapeit;

import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import static com.example.escapeit.LoginActivity.UIHandler;

public class Riddler extends AppCompatActivity implements View.OnClickListener {


    TextView total;
    ProgressBar progressBar;
    int riddlercounter = 0;
    int totalplayers;
    int gameID;
    int playerID;
    private Handler progressBarHandler = new Handler();
    private Socket mSocket;

    private Button QRButton;
    static final int SCAN_REQUEST = 1;

    static final int RIDDLER1_REQUEST = 10;


    /*private String[] riddleArray = {"Bet you can't resist me on a hot summer day. I can make you burn and feel cold at the same time, I dare say. ",
    "Find the next clue near deep water. Man walks over, man walks under, in times of war he burns asunder?",
    "Where each turn finds you in a new mystery, a new adventure, or a new romance. "};*/



    int totalplayerslogedin = 0;
    int round;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_riddler);

        Button googleMapsButton = (Button) findViewById(R.id.buttonGoogleRiddler);
        googleMapsButton.setOnClickListener(this);

        QRButton = (Button) findViewById(R.id.buttonQRRiddler);
        QRButton.setOnClickListener(this);

        //extract cookie data
        Intent i = getIntent();
        totalplayers = i.getIntExtra("totalplayers", 0);
        gameID = i.getIntExtra("gameid", 0);
        playerID = i.getIntExtra("playerid", 0);

        //set progressbar
        total = findViewById(R.id.TotalRiddler);
        progressBar = findViewById(R.id.RiddlerProgressBar);
        progressBar.setProgress(0);
        progressBar.setMax(totalplayers);
        progressBar.setVisibility(View.VISIBLE);


        mSocket = SocketClass.getSocket();
        mSocket.on("player_riddler_login", onNewLogin);
        mSocket.on("riddler_puzzle_start", onPuzzleStart);

        startRiddler();

    }

    static {
        UIHandler = new Handler(Looper.getMainLooper());
    }

    public static void runOnUI(Runnable runnable) {
        UIHandler.post(runnable);
    }

    private Emitter.Listener onNewLogin = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject data = (JSONObject) args[0];

            try {
                totalplayerslogedin = data.getInt("total_players_logged_in");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            progressBarHandler.post(new Runnable() {
                public void run() {
                    total.setText(totalplayerslogedin + "/" + totalplayers);
                    progressBar.setProgress(0);
                    progressBar.setMax(totalplayers);
                    progressBar.setProgress(riddlercounter);
                }
            });

        }
    };

    private Emitter.Listener onPuzzleStart = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject data = (JSONObject) args[0];


            //AT SERVER IF PUZZLE = STARTED; ANSWER 1ON1!
            //GET ON WHICH RIDDLE THEY ARE via object

            try {
               round = data.getInt("riddler_round");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            startRiddler();

        }
    };

    private void startRiddler(){
        Intent in = new Intent(Riddler.this, Riddler2.class);
        in.putExtra("round", round);
        in.putExtra("playerid", playerID);
        in.putExtra("gameid", gameID);
        startActivityForResult(in, RIDDLER1_REQUEST);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonGoogleRiddler:
                double destinationLatitude = 50.930103;
                double destinationLongitude = 5.338349;
                String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%f,%f (%s)", destinationLatitude, destinationLongitude, "RIDDLERLOCATION");
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
                break;
            case R.id.buttonQRRiddler:
                Intent i = new Intent(this, QR.class);
                startActivityForResult(i, SCAN_REQUEST);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SCAN_REQUEST) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(getApplicationContext(),"QR of riddler puzzle has been registered",Toast.LENGTH_SHORT).show();
                QRButton.setClickable(false);

                JSONObject RiddlerPuzzleJSON = new JSONObject();
                try {
                    RiddlerPuzzleJSON.put("game_id", gameID);
                    RiddlerPuzzleJSON.put("player_id", playerID);
                    RiddlerPuzzleJSON.put("puzzle_name", "riddler");
                } catch (JSONException e) {
                }
                mSocket.connect();
                mSocket.emit("player_registration_puzzle", RiddlerPuzzleJSON.toString());

            }
        }
        if (requestCode == RIDDLER1_REQUEST) {
            if (resultCode == RESULT_OK) {
                Intent in = new Intent();
                setResult(RESULT_OK, in);
                finish();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
        mSocket.off("player_riddler_login", onNewLogin);
        mSocket.off("riddler_puzzle_start", onPuzzleStart);

    }
}