package com.example.escapeit;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class APICalls {

    public static String HttpPost(String myUrl, JSONObject obj) throws IOException {
        //try{
            URL url = new URL(myUrl);

        // 1. create HttpURLConnection
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");

        // 2. build JSON object //done @location
        // 3. add JSON content to POST request body
        setPostRequestContent(conn, obj);

        // 4. make POST request to the given URL
        conn.connect();

        // 5. return response message
        //int yo = conn.getResponseCode();
      //  Log.d("myTag", yo);
        InputStream is = null;
        try {
            is = conn.getInputStream();
            int ch;
            StringBuffer sb = new StringBuffer();
            while ((ch = is.read()) != -1) {
                sb.append((char) ch);
            }
            return sb.toString();
        } catch (IOException e) {
            throw e;
        } finally {
            if (is != null) {
                is.close();
            }
        }

        //return conn.getResponseMessage()+"" + conn.getResponseCode() + " " + conn.getContent();
        //}catch(IOException e){return "IO failed";}

    }
    public static String HttpPostCode(String myUrl, JSONObject obj) throws IOException {
        //try{
        URL url = new URL(myUrl);

        // 1. create HttpURLConnection
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");

        // 2. build JSON object //done @location
        // 3. add JSON content to POST request body
        setPostRequestContent(conn, obj);

        // 4. make POST request to the given URL
        conn.connect();

        // 5. return response message
        //String yo = conn.getResponseMessage()
        return conn.getResponseCode()+ "" ;
    }
    private static void setPostRequestContent(HttpURLConnection conn, JSONObject jsonObject) throws IOException {

        OutputStream os = conn.getOutputStream();
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
        writer.write(jsonObject.toString());
        Log.i(MainActivity.class.toString(), jsonObject.toString());
        writer.flush();
        writer.close();
        os.close();
    }


}
