package com.example.escapeit;


import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;

public class GameSocket {

    private static Socket mSocket;
    private GameSocket(){}  //private constructor.

    public static Socket getInstance(String name){
        if (mSocket == null){ //if there is no instance available... create new one
            String combined = "http://192.168.16.175:5000/test/"+name;
            try {
                mSocket = IO.socket(combined);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
        return mSocket;
    }
}
