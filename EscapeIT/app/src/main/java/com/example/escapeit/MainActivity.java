package com.example.escapeit;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import static com.example.escapeit.LoginActivity.UIHandler;

public class MainActivity extends AppCompatActivity {

    int totalplayers = 10;
    int gameID = 0;
    int playerID = 0;
    private Socket mSocket;
    private Handler progressBarHandler = new Handler();

    private boolean completeLaser= false;
    private boolean completeRiddler = false;
    private boolean completeExplosion = false;
    private boolean completeQuiz = false;

    static final int LASER_REQUEST= 1;
    static final int RIDDLER_REQUEST = 2;
    static final int EXPLOSION_REQUEST = 3;
    static final int QUIZ_REQUEST = 4;

    private int totalpoints = 0;
    private TextView pointsView;
    int points = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent i = getIntent();
        totalplayers = i.getIntExtra("totalplayers",0);
        gameID = i.getIntExtra("gameid",0);
        playerID = i.getIntExtra("playerid",0);

        //GET AND FILL COMLETED PUZZLES bools
        //GET AND FILL POINTS view

        mSocket = SocketClass.getSocket();
        mSocket.on("player_receive_points", onReceivePoints);


        pointsView = findViewById(R.id.textView14);

        GridLayout myGridLayout = findViewById(R.id.GridLayout1);
        int childCount = myGridLayout.getChildCount();
        for (int x= 0; x < childCount; x++){
            ImageView container = (ImageView) myGridLayout.getChildAt(x);
            container.setOnClickListener(new View.OnClickListener(){
                public void onClick(View view){
                    if(view.getId() == R.id.Laser){
                        if(!completeLaser) {
                            Intent i = new Intent(MainActivity.this, Laser.class);
                            i.putExtra("playerid", playerID);
                            i.putExtra("gameid", gameID);
                            i.putExtra("totalplayers", totalplayers);
                            startActivityForResult(i, LASER_REQUEST);
                        }
                        else{
                            Toast.makeText(getApplicationContext(),"Already completed the laser game",Toast.LENGTH_SHORT).show();
                        }
                    }
                    if(view.getId() == R.id.Riddler){
                        if(!completeRiddler) {
                            Intent i = new Intent(MainActivity.this, Riddler.class);
                            i.putExtra("playerid", playerID);
                            i.putExtra("gameid", gameID);
                            i.putExtra("totalplayers", totalplayers);
                            startActivityForResult(i, LASER_REQUEST);
                        }
                        else{
                            Toast.makeText(getApplicationContext(),"Already completed the riddler game",Toast.LENGTH_SHORT).show();
                        }
                    }
                    if(view.getId() == R.id.quiz){
                        if(!completeQuiz) {
                            Intent i = new Intent(MainActivity.this, Quiz.class);
                            i.putExtra("playerid", playerID);
                            i.putExtra("gameid", gameID);
                            i.putExtra("totalplayers", totalplayers);
                            startActivityForResult(i, LASER_REQUEST);
                        }
                        else{
                            Toast.makeText(getApplicationContext(),"Already completed the quiz game",Toast.LENGTH_SHORT).show();
                        }
                    }
                    if(view.getId() == R.id.explosion){
                        if(!completeExplosion) {
                            Intent i = new Intent(MainActivity.this, Explosion.class);
                            i.putExtra("playerid", playerID);
                            i.putExtra("gameid", gameID);
                            i.putExtra("totalplayers", totalplayers);
                            startActivityForResult(i, LASER_REQUEST);
                        }
                        else{
                            Toast.makeText(getApplicationContext(),"Already completed the explosion game",Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            });
        }
    }

    static {
        UIHandler = new Handler(Looper.getMainLooper());
    }
    public static void runOnUI(Runnable runnable) {
        UIHandler.post(runnable);
    }

    private Emitter.Listener onReceivePoints = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject data = (JSONObject) args[0];

            try {
                int points = data.getInt("total_points");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            progressBarHandler.post(new Runnable() {
                public void run() {
                    pointsView.setText(Integer.toString(points));
                }
            });
        }
    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LASER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(getApplicationContext(),"Completed the laser game",Toast.LENGTH_SHORT).show();
                completeLaser = true;
        }
        if (requestCode == RIDDLER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(getApplicationContext(),"Completed the riddler game",Toast.LENGTH_SHORT).show();
                completeLaser = true;
            }
        }
        if (requestCode == QUIZ_REQUEST) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(getApplicationContext(),"Completed the quiz game",Toast.LENGTH_SHORT).show();
                completeLaser = true;
            }
        }
        if (requestCode == EXPLOSION_REQUEST) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(getApplicationContext(),"Completed the explosion game",Toast.LENGTH_SHORT).show();
                completeLaser = true;
            }
        }
        JSONObject pointsJSON = new JSONObject();
            try {
                JSONObject playerJson = new JSONObject();
                playerJson.put("game_id", gameID);
                playerJson.put("player_id", playerID);
                pointsJSON.put("player", playerJson);
            } catch(JSONException e){ }
            mSocket.emit("player_ask_points", pointsJSON.toString());
        }
    }

    private Emitter.Listener onPointsChange = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject data = (JSONObject) args[0];

            try {
                totalpoints = data.getInt("total_points");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            pointsView.setText( "points: " + totalpoints);
        }
    };
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
        mSocket.off("player_points", onPointsChange);

    }
}
